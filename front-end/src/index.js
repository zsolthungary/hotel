import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

if (process.env.NODE_ENV !== 'production') {
    axios.defaults.baseURL = 'http://localhost:8080';
} else {
    axios.defaults.baseURL = 'http://stackmen.progmasters.hu:8080';
}

/*axios.defaults.headers.post['xsrfCookieName'] = 'CSRFToken';
axios.defaults.headers.post['xsrfHeaderName'] = 'X-CSRFToken';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';*/
axios.defaults.withCredentials = true;


ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();