import React from "react";

function ReservationForm(props) {

    return (

        <div>
            <h2>Make a reservation</h2>
            <hr/>
            <br/>
            <form onSubmit={props.reservationSubmit}>
                <div className="form-group">
                    <label>Guest name:</label>
                    <input type="text"
                           className={props.reservationFormData.guestName.isValid ? "form-control" : "form-control is-invalid"}
                           name="guestName"
                           placeholder={JSON.parse(localStorage.getItem("user")).name}
                           value={props.reservationFormData.guestName.value}
                           onChange={props.inputChangeHandler}
                    />
                    <small className="form-text text-danger">{props.reservationFormData.guestName.message}</small>
                </div>

                < div className="form-group">
                    <label>Check-in and check-out dates: </label>
                    <input type="date"
                           name="startDate"
                           value={props.reservationFormData.startDate.value}
                           onChange={props.inputChangeHandler}
                    />
                    <span> — </span>
                    <input type="date"
                           name="endDate"
                           value={props.reservationFormData.endDate.value}
                           onChange={props.inputChangeHandler}
                    />
                    <small
                        className="form-text text-danger">{props.reservationFormData.startDate.message}</small>
                    <small
                        className="form-text text-danger">{props.reservationFormData.endDate.message}</small>
                </div>
                <button className="btn btn-primary my-buttons" type="submit">Reserve room</button>
            </form>
        </div>
    )
}

export default ReservationForm;