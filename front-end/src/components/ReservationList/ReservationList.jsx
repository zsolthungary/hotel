import React from 'react';
import ReservationListItem from '../ReservationListItem/ReservationListItem';

function ReservationList(props) {
    const reservations = props.reservations.map(reservation => {
        return <ReservationListItem
            key={reservation.id}
            id={reservation.id}
            guestName={reservation.guestName}
            startDate={reservation.startDate}
            endDate={reservation.endDate}
            reservationDelete={props.reservationDelete}
            reservationEdit={props.reservationEdit}
            checkAdminRole={props.checkAdminRole}
        />
    });

    return (
        <div className="row">
            <div className="col">
                <br/>
                <br/>
                <table className="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Guest name</th>
                        <th>Reservation date</th>
                        <th>Edit reservation</th>
                        <th>Delete reservation</th>
                    </tr>
                    </thead>
                    <tbody>
                    {reservations}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default ReservationList;