import React from 'react';
import "./days.css"
import "./day-reservation.css"

function CalendarListItem(props) {


    return (
        <tr>
            <td
                onClick={() => props.listener(props, 0)}
                className={props.weekDays[0][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[0][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[0][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[0][0]}
            </td>

            <td onClick={() => props.listener(props, 1)}
                className={props.weekDays[1][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[1][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[1][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[1][0]}
            </td>

            <td onClick={() => props.listener(props, 2)}
                className={props.weekDays[2][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[2][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[2][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[2][0]}
            </td>

            <td onClick={() => props.listener(props, 3)}
                className={props.weekDays[3][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[3][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[3][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[3][0]}
            </td>

            <td onClick={() => props.listener(props, 4)}
                className={props.weekDays[4][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[4][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[4][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[4][0]}
            </td>

            <td onClick={() => props.listener(props, 5)}
                className={props.weekDays[5][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[5][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[5][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[5][0]}
            </td>

            <td onClick={() => props.listener(props, 6)}
                className={props.weekDays[6][1].includes("R") ? "reserved-day days" : "days" |
                props.weekDays[6][1].includes("C") ? "choosen-day" : "days" |
                props.weekDays[6][1].includes("N") ? "previous" : "days"}>
                {props.weekDays[6][0]}
            </td>
        </tr>


    )
}

export default CalendarListItem;