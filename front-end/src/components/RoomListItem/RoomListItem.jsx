import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import './RoomListItem.css';

function RoomListItem(props) {

    return (
        <Fragment>
            <div>
                <div className="row">
                    <div className="col-auto">
                        <Link to={'/rooms/' + props.id}>
                            <img className="room-list-item__image"
                                 src={(props.imageUrl && props.imageUrl.trim() !== "") ? props.imageUrl : "/images/noimage.png"}
                                 alt="Room"/>
                        </Link>
                    </div>
                    <div className="col text-left">
                        <h4><Link to={'/hotels/' + props.hotelId}>{props.hotelName}</Link></h4>
                        <p><strong>Room: </strong> <Link to={'/rooms/' + props.id}>{props.name}</Link></p>
                        <p><strong>Number of people: </strong> {props.numberOfBeds}</p>
                        <p><strong>Price per night: </strong> {props.pricePerNight}</p>
                    </div>
                </div>
            </div>
        </Fragment>

    )
}

export default RoomListItem;