import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import './CommentList.css'

function CommentList(props) {

    const listComments = () => {

        return (
            props.comments.map((comment, index) => {
                    return (
                        <div key={index} className={index % 2 === 0 ? "table-secondary" : "table-default"}>
                            <div className="row">
                                <div className="col-sm text-left paddingTop">
                                    <strong>Author:  </strong>{comment.author}
                                </div>
                                <div className="col-sm text-right paddingTop">
                                    <strong>Rating:  </strong>
                                </div>
                                <div className="col-sm text-left">
                                    <StarRatingComponent
                                        name="rating"
                                        editing={false}
                                        starCount={5}
                                        starColor='#009999'
                                        value={comment.rating}
                                    />
                                </div>
                                <div className="col-sm-5 text-right paddingTop">
                                    Date: {comment.createdAt}
                                </div>
                            </div>
                            <p className="text-justify"><strong>Review: </strong>{comment.text}</p>
                        </div>
                    )
                }
            ))
    };

    return (
        <div>
           {listComments()}
        </div>
    );
}

export default CommentList;