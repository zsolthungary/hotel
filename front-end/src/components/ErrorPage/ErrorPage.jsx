import React, {Component} from 'react';
import './errorPage.css';

class ErrorPage extends Component {

    render() {
        return (
            <div>
                <div className="errorPage_number">
                    404
                </div>
                <div className="errorPage_title">
                    <p> Ooops, something went wrong. </p>
                </div>
                <div className="errorPage_text">
                    <p>You can go back by using the navbar.</p>
                </div>
            </div>
        )
    }
}

export default ErrorPage;
