import React from 'react';

function MyReservationListItem(props) {


    return (
        <tr className="table-cell">
            <td>{props.hotelName}</td>
            <td>{props.roomName}</td>
            <td>{props.startDate}</td>
            <td>{props.endDate}</td>
            <td>
                <button type="button" onClick={() => props.editReservation(props.reservationId)}
                        className="btn btn-outline-success" data-target="#exampleModal2">
                    Edit
                </button>
            </td>

            <td>

                <button type="button" className="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal2">
                    Delete
                </button>

                <div className="modal fade" id="exampleModal2" tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel2">Delete reservation?</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                Are you sure you want to delete this reservation?
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    data-dismiss="modal"
                                    onClick={() => props.deleteReservation(props.reservationId)}>
                                    Yes
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    )
}

export default MyReservationListItem;