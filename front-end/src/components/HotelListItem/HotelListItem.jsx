import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import './HotelListItem.css';
import StarRatingComponent from "react-star-rating-component";

function HotelListItem(props) {

    return (
        <Fragment>
                <div className="row">
                    <div className="col-auto">
                        <Link to={'/hotels/' + props.id}>
                            <img className="hotel-list-item__image"
                                 src={(props.imageUrl && props.imageUrl.trim() !== "") ? props.imageUrl : "/images/noimage.png"}
                                 alt="Hotel"/>
                        </Link>
                    </div>
                    <div className="col text-left">
                        <div className="row">
                            <div className="col-sm text-left">
                                <h5><strong>Hotel: </strong> <Link to={'/hotels/' + props.id}>{props.name}</Link></h5>
                                <p className="data"><strong>Address: </strong>{props.address}</p>
                                <p className="data"><strong>Number of rooms: </strong> {props.numberOfRooms}</p>
                                {props.hasEmptyRoom ? null : <p className="form-text text-danger"><strong>You missed it, no room available</strong></p>}
                            </div>
                            <div className="col-sm text-right noPadding">
                                <StarRatingComponent
                                    name="rating"
                                    editing={false}
                                    starCount={1}
                                    starColor='#EEC41C'
                                    value={props.averageRating !== 0 ? 1 : 0}
                                />
                            </div>
                            <div className="col-sm-1.5 text-left rating">
                                {props.averageRating !== 0 ? props.averageRating + '/5' : 'Not Rated'}
                            </div>
                        </div>

                    </div>
                </div>
        </Fragment>

    )
}

export default HotelListItem;