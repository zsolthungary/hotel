import React, {Fragment} from 'react';
import axios from "axios";

function HotelAction(props) {

    const checkAdminRole = () => {
        let roleListener = false;
        if (localStorage.getItem("user")) {
            const item = JSON.parse(localStorage.getItem("user"));
            const role = item.role.substring(5);
            if (role === "ADMIN") {
                roleListener = true;
            }
        }
        return roleListener;
    };

    const addRoom = () => {
        props.history.push("/register-room/" + props.hotelId)
    };

    const editHotel = () => {
        props.history.push('/edit-hotel/' + props.hotelId);
    };

    const deleteHotel = (id) => {
        axios.delete('/api/hotels/' + id)
            .then(response => {
                props.history.push('/')
            })
            .catch(error => {
                console.log(error.response)
            })
    };

    return (
        <Fragment>
            {checkAdminRole() ?
                <div>
                    <div className="hotelButtonContainer">
                        <button type="button"
                                onClick={() => addRoom()}
                                className="btn btn-outline-primary button hotel-buttons">Add room
                        </button>

                        <button
                            type="button" className="btn btn-outline-success hotel-buttons"
                            onClick={() => editHotel()}>
                            Edit hotel
                        </button>

                        <button
                            type="button" className="btn btn-outline-danger hotel-buttons" data-toggle="modal"
                            data-target="#exampleModal5">
                            Delete hotel
                        </button>
                    </div>
                    <div className="modal fade" id="exampleModal5" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Delete hotel</h5>
                                    <button type="button" className="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    Are you sure you want to delete this hotel?
                                </div>
                                <div className="modal-footer">
                                    <button
                                        type="button"
                                        className="btn btn-primary"
                                        data-dismiss="modal"
                                        onClick={() => deleteHotel(props.hotelId)}
                                    >
                                        Yes
                                    </button>
                                    <button
                                        type="button"
                                        className="btn btn-secondary"
                                        data-dismiss="modal"
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null}
        </Fragment>
    )
}

export default HotelAction;