import React from 'react';
import {Link} from 'react-router-dom';
import './NavBar.css'

function UnRegisteredNavBar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container">
                <Link className="navbar-brand hotel" to="/">HOTEL</Link>
                <button type="button" className="navbar-toggler collapsed" data-toggle="collapse"
                        data-target="#myNavbar"
                        aria-expanded="false" aria-controls="myNavbar" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div id="myNavbar" className="navbar-collapse collapse menu">
                    <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/">Hotel List</Link></li>
                    </ul>
                    <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/registration">Registration</Link></li>
                    </ul>
                    <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/login">Login</Link></li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default UnRegisteredNavBar;