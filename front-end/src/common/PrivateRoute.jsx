import React from 'react';
import {Route, Redirect} from 'react-router-dom';

function PrivateRoute({component: Component, ...rest}) {
    return (
        <Route {...rest} render={(props) => (
            localStorage.auth === 'true'
                ? <Component {...props} />
                : <Redirect to='/registration'/>
        )}/>
    )
}

export default PrivateRoute;