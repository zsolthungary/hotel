import React from 'react';
import {Route, Switch} from 'react-router-dom';
import RoomList from '../containers/RoomList/RoomList';
import RoomDetails from '../containers/RoomDetails/RoomDetails';
import NavBar from './NavBar'
import RoomForm from "../containers/RoomForm/RoomForm";
import HotelForm from "../containers/HotelForm/HotelForm";
import HotelDetails from "../containers/HotelDetails/HotelDetails";
import LoginForm from "../containers/Login/LoginForm";
import RegistrationForm from "../containers/RegistrationForm/RegistrationForm";
import UnRegisteredNavBar from "./UnRegisteredNavBar";
import Logout from "../containers/Logout/Logout";
import EditReservationForm from "../containers/EditReservationForm/EditReservationForm";
import HotelList from "../containers/HotelList/HotelList";
import MyReservations from "../containers/MyReservations/MyReservations";
import ErrorPage from "../components/ErrorPage/ErrorPage";

function Layout() {
    return (
        <div>
            {localStorage.getItem("user") ? <NavBar/> : <UnRegisteredNavBar/>}
            <div className="container">
                <div className="jumbotron">
                    <Switch>
                        <Route path="/" exact component={HotelList}/>
                        <Route path="/rooms" exact component={RoomList}/>
                        <Route path="/rooms/:id" exact component={RoomDetails}/>
                        <Route path="/hotels/:id" exact component={HotelDetails}/>
                        <Route path="/register-room" exact component={RoomForm}/>
                        <Route path="/register-room/:hotelId" exact component={RoomForm}/>
                        <Route path="/edit-room/:roomId" exact component={RoomForm}/>
                        <Route path="/register-hotel" exact component={HotelForm}/>
                        <Route path="/edit-hotel/:hotelId" exact component={HotelForm}/>
                        <Route path="/login" exact component={LoginForm}/>
                        <Route path="/logout" exact component={Logout}/>
                        <Route path="/registration" exact component={RegistrationForm}/>
                        <Route path="/edit/:userId" exact component={RegistrationForm}/>
                        <Route path="/edit-reservation/:id" exact component={EditReservationForm}/>
                        <Route path="/my-reservations" exact component={MyReservations}/>
                        <Route path='*' exact={true} component={ErrorPage}/>
                    </Switch>
                </div>
            </div>
        </div>
    )
}

export default Layout;