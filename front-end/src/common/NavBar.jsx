import React from 'react';
import {Link} from 'react-router-dom';
import './NavBar.css'

function NavBar() {
    const checkUserRole = () => {
        const item = JSON.parse(localStorage.getItem("user"));
        const role = item.role.substring(5);
        let roleListener = false;
        if(role === "USER"){
            roleListener = true;
        }
        return roleListener;
    };

    const checkAdminRole = () => {
        const item = JSON.parse(localStorage.getItem("user"));
        const role = item.role.substring(5);
        let roleListener = false;
        if(role === "ADMIN"){
            roleListener = true;
        }
        return roleListener;
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container">
                <Link className="navbar-brand hotel" to="/">HOTEL</Link>
                <button type="button" className="navbar-toggler collapsed" data-toggle="collapse"
                        data-target="#myNavbar"
                        aria-expanded="false" aria-controls="myNavbar" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div id="myNavbar" className="navbar-collapse collapse menu">
                    <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/">Hotel List</Link></li>
                    </ul>
                    {checkAdminRole() ?
                    <ul className="navbar-nav mt-2 mt-lg-0">

                        <li className="nav-item"><Link className="nav-link" to="/register-hotel">New Hotel</Link></li>

                    </ul> : ""}
                    {checkAdminRole() ?
                        <ul className="navbar-nav mt-2 mt-lg-0">

                        <li className="nav-item"><Link className="nav-link" to="/register-room">New Room</Link></li>
                    </ul> : ""}
                    {checkUserRole() ? <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/my-reservations">My Reservations</Link></li>
                        {/*<li className="nav-item"><Link className="nav-link" to="/edit/:userId">My Account</Link></li>*/}
                    </ul> : ""}
                    <ul className="navbar-nav mt-2 mt-lg-0">
                        <li className="nav-item"><Link className="nav-link" to="/logout">Logout</Link></li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default NavBar;