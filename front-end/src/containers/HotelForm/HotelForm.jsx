import React, {Component, Fragment} from "react";
import axios from "axios";
import "./HotelForm.css"

class HotelForm extends Component {

    state = {
        globalMessage: null,
        formData: {
            name: {
                value: "",
                isValid: true,
                message: null
            },
            address: {
                value: "",
                isValid: true,
                message: null
            },
            description: {
                value: "",
                isValid: true,
                message: null
            },
            imageUrls: {
                value: [],
                isValid: true,
                message: null
            }
        }
    };


    componentDidMount() {
        if (!localStorage.getItem("user")) {
            this.props.history.push("/login");
        }

        if (this.props.match.params.hotelId) {
            axios.get('/api/hotels/' + this.props.match.params.hotelId)
                .then(response => {
                    const updatedHotelForm = {
                        ...this.state.formData
                    };

                    for (let field in response.data) {
                        const updatedFormElement = {
                            ...updatedHotelForm[field]
                        };
                        updatedFormElement.value = response.data[field];
                        updatedHotelForm[field] = updatedFormElement;
                    }

                    this.setState({formData: updatedHotelForm});
                })
        }
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.match.params.hotelId && !this.props.match.params.hotelId) {
            const updatedHotelForm = {...this.state.formData};
            let imageUrls = updatedHotelForm.imageUrls;

            for (let field in updatedHotelForm) {
                updatedHotelForm[field].value = "";
            }

            imageUrls.value = [];
            updatedHotelForm.imageUrls = imageUrls;

            this.setState({
                formData: updatedHotelForm
            });
        }
    }


    formSubmit = event => {
        event.preventDefault();

        const data = {};
        for (let formElementIdentifier in this.state.formData) {
            data[formElementIdentifier] = this.state.formData[formElementIdentifier].value;
        }

        let url = "/api/hotels";
        let method = "post";
        // let path = '';

        if (this.props.match.params.hotelId) {

            url += "/" + this.props.match.params.hotelId;
            method = "put";
        }

        axios({method: method, url: url, data: data})
            .then((response) => {
                this.props.history.push("/hotels/" + response.data);
            })
            .catch(error => {
                console.warn("error.response: ", error === null ? error.response : error);
                this.validationHandler(error);
            });
    };

    validationHandler = error => {
        const responseData = error.response.data;
        const updatedFormData = {...this.state.formData};
        if (responseData !== null) {
            if (responseData.globalError) {
                this.setState({globalMessage: responseData.globalError});
            }
            if (responseData.fieldErrors) {
                for (let fe of responseData.fieldErrors) {
                    updatedFormData[fe.field] = {
                        value: this.state.formData[fe.field].value,
                        isValid: false,
                        message: fe.message
                    }
                }
            }
            this.setState({formData: updatedFormData});
        } else {
            this.setState({globalMessage: error});
        }
    };

    inputChangeHandler = event => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            message: null
        };

        this.setState({formData: updatedFormData})
    };

    uploadWidget = () => {
        window.cloudinary.openUploadWidget({cloud_name: 'progmasters', upload_preset: 'ffrm3vqx'},
            (error, result) => {
                if (!error && result && result.event === "success") {
                    // console.log('Done! Here is the image info: ', result.info);
                    let updateFormData = {...this.state.formData};
                    let updateImageUrls = updateFormData.imageUrls;
                    updateImageUrls.value = updateImageUrls.value.concat(result.info.secure_url);

                    updateFormData.imageUrls = updateImageUrls;

                    this.setState({
                        formData: updateFormData
                    })
                } else if (error) {
                    console.warn("error.response: ", error);
                }
            });
    };

    showImages = () => {

        return (
            <div>
                {this.state.formData.imageUrls.value.length !== 0 ? <label><strong>Image(s):</strong></label> : null}
                <br/>
                {this.state.formData.imageUrls.value.map((image, index) => {
                    return (
                        image !== null ?
                            <img src={image} key={index} className="hotel-image" alt="Hotel"/>
                            : null
                    )
                })}
            </div>
        )
    };

    render() {
        return (
            <Fragment>
                <h2><strong>{this.props.match.params.hotelId ? "Edit Hotel" : "Register New Hotel"}</strong></h2>
                <hr/>
                {this.state.globalMessage ?
                    <div className="alert alert-warning">{this.state.globalMessage}</div> : ""}
                <form onSubmit={this.formSubmit}>
                    <div className="form-group">
                        <label><strong>Hotel name:</strong></label>
                        <input
                            type="text"
                            className={this.state.formData.name.isValid ? "form-control" : "form-control is-invalid"}
                            name="name"
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.name.value}
                            placeholder="Enter Hotel name"
                        />
                        <small className="form-text text-danger">{this.state.formData.name.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Hotel address:</strong></label>
                        <input
                            type="text"
                            className={this.state.formData.address.isValid ? "form-control" : "form-control is-invalid"}
                            name="address"
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.address.value}
                            placeholder="Enter Hotel address"
                        />
                        <small className="form-text text-danger">{this.state.formData.address.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Description:</strong></label>
                        <textarea
                            className={this.state.formData.description.isValid ? "form-control" : "form-control is-invalid"}
                            name="description"
                            onChange={this.inputChangeHandler}
                            placeholder="Enter Hotel description"
                            rows="4"
                            value={this.state.formData.description.value}
                        />
                        <small className="form-text text-danger">{this.state.formData.description.message}</small>
                    </div>
                    <div className="form-group">
                        <div>
                            {this.showImages()}
                        </div>
                        <hr/>
                    </div>
                    <button type="button" onClick={this.uploadWidget.bind(this)} className="btn btn-primary button">
                        Add Images
                    </button>
                    <button type="submit" className="btn btn-primary button">
                        {this.props.match.params.hotelId ? "Save Changes" : "Save Hotel"}
                    </button>
                </form>
            </Fragment>
        )
    }
}

export default HotelForm;