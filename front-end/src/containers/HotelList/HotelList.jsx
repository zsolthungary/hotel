import React, {Component, Fragment} from 'react';
import moment from 'moment';
import axios from 'axios';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import {formatDate, parseDate} from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';
import './HotelList.css';
import HotelListItem from "../../components/HotelListItem/HotelListItem";

class HotelList extends Component {

    constructor(props) {
        super(props);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
    }

    state = {
        hotels: [],
        formData: {
            search: {
                value: "",
                isValid: true,
                message: ""
            }
        },
        from: undefined,
        to: undefined
    };

    getHotels = () => {

        let url = '/api/hotels';
        if (this.state.formData.search.value !== "") {
            url += '?search=' + this.state.formData.search.value;

            if (this.state.from !== undefined && this.state.to !== undefined) {
                url += '&from=' + this.formDate(this.state.from) + '&to=' + this.formDate(this.state.to);
            }
        }

        axios.get(url)
            .then(response => {
                this.setState(
                    response.data
                );
            })
            .catch(error => {
            });
    };

    formDate = (date) => {
        let dd = String(date.getDate()).padStart(2, '0');
        let mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = date.getFullYear();

        return yyyy + "-" + mm + "-" + dd;
    }

    componentDidMount() {
        this.getHotels();
    }

    inputChangeHandler = event => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            message: null
        };

        this.setState({formData: updatedFormData})
    };

    handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            this.getHotels();
        }
    }

    checkAdminRole = () => {
        let roleListener = false;
        if (localStorage.getItem("user")) {
            const item = JSON.parse(localStorage.getItem("user"));
            const role = item.role.substring(5);
            if (role === "ADMIN") {
                roleListener = true;
            }
        }
        return roleListener;
    };

    showFromMonth = () => {
        const {from, to} = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }

    handleFromChange = (from) => {
        // Change the from date and focus the "to" input field
        this.setState({from});
    }

    handleToChange = (to) => {
        this.setState({to}, this.showFromMonth);
    }

    render() {
        const {from, to} = this.state;
        const modifiers = {start: from, end: to};

        const hotels = this.state.hotels.map(hotel => {
            return (<HotelListItem
                    key={hotel.id}
                    id={hotel.id}
                    name={hotel.name}
                    address={hotel.address}
                    imageUrl={hotel.imageUrl}
                    numberOfRooms={hotel.numberOfRooms}
                    averageRating={hotel.averageRating}
                    hasEmptyRoom={hotel.hasEmptyRoom}
                />
            )
        });

        return (
            <Fragment>
                <div className="form-inline search-box">
                    <label><h2 className="title">
                        <strong>{this.checkAdminRole() ? "Find your Hotel!" : "Find your stay!"}</strong></h2>
                    </label>
                    <input
                        type="text"
                        className={this.state.formData.search.isValid ? "form-control search-input" : "form-control is-invalid"}
                        name="search"
                        onChange={this.inputChangeHandler}
                        onKeyDown={this.handleKeyDown}
                        value={this.state.formData.search.value}
                        placeholder={this.checkAdminRole() ? "" : "Where do you want to go?"}
                    />
                    <div className="InputFromTo">
                        <DayPickerInput
                            inputProps={{className: "day-picker"}}
                            value={from}
                            placeholder="From"
                            format="YYYY-MM-DD"
                            formatDate={formatDate}
                            parseDate={parseDate}
                            dayPickerProps={{
                                selectedDays: [from, {from, to}],
                                disabledDays: {after: to},
                                toMonth: to,
                                modifiers,
                                numberOfMonths: 2,
                                onDayClick: () => this.to.getInput().focus(),
                            }}
                            onDayChange={this.handleFromChange}
                        />
                    </div>
                    <div className="InputFromTo InputFromTo-to">
                        <DayPickerInput
                            inputProps={{className: "day-picker"}}
                            ref={el => (this.to = el)}
                            value={to}
                            placeholder="To"
                            format="YYYY-MM-DD"
                            formatDate={formatDate}
                            parseDate={parseDate}
                            dayPickerProps={{
                                selectedDays: [from, {from, to}],
                                disabledDays: {before: from},
                                modifiers,
                                month: from,
                                fromMonth: from,
                                numberOfMonths: 2,
                            }}
                            onDayChange={this.handleToChange}
                        />
                    </div>
                    <button
                        className="btn btn-primary button button-search"
                        onClick={this.getHotels}
                    >Search
                    </button>

                </div>
                < small
                    className="form-text text-danger"> {this.state.formData.search.message}
                </small>
                <hr/>
                <br/>
                <div className="container">
                    {this.state.formData.search.value !== "" && this.state.hotels.length === 0 ?
                        <h3>No Hotel found</h3> : hotels}
                </div>
            </Fragment>
        )
    }
}


export default HotelList;