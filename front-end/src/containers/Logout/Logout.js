import React, { Component } from 'react'
// import { Redirect } from 'react-router-dom'
import axios from "axios";

class Logout extends Component {
    componentDidMount() {
        axios.post('/logout')
            .then(response => {
                localStorage.removeItem('user');
                this.props.history.push('/login')
            })
            .catch(error => {
                console.log(error.response);
            });
    }

    render() {
        // return <Redirect to='/login' />
        return <h3>You have successfully logged out</h3>
    }
}

export default Logout;