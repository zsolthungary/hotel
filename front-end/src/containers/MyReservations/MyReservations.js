import React, {Component} from 'react';
import axios from 'axios';
import MyReservationListItem from "../../components/MyReservationsListItem/MyReservationsListItem";

class MyReservations extends Component {

    state = {
        myReservations: [],
    };

    componentDidMount() {
        this.getMyReservations();
    }

    getMyReservations = () => {
        const item = JSON.parse(localStorage.getItem("user"));
        const userName = item.name;

        axios.get("/api/rooms/myReservations/" + userName)
            .then(response => {
                console.log(response.data);
                this.setState({
                    myReservations: response.data
                });
            });
    };

    editReservation = (id) => {
        this.props.history.push('/edit-reservation/' + id);
    };

    deleteReservationHandler = (id) => {
        axios.delete('/api/rooms/myReservations/' + id)

            .then(response => {
                this.getMyReservations();
                this.props.history.push('/my-reservations')
            })
            .catch(error => {
                console.warn(error.response);
            });

    };

    render() {
        const myReservations = this.state.myReservations.map(myReservation => {
            return (
                <MyReservationListItem
                    key={myReservation.reservationId}
                    reservationId={myReservation.reservationId}
                    hotelName={myReservation.hotelName}
                    roomName={myReservation.roomName}
                    startDate={myReservation.startDate}
                    endDate={myReservation.endDate}
                    editReservation={this.editReservation}
                    deleteReservation={this.deleteReservationHandler}
                />)
        });

        return (
            <div className="row">
                <div className="col">
                    <hr/>
                    <table className="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Hotel name</th>
                            <th>Room name</th>
                            <th>Start date</th>
                            <th>End date</th>
                            <th>Edit reservation</th>
                            <th>Delete reservation</th>
                        </tr>
                        </thead>
                        <tbody>
                        {myReservations}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default MyReservations;