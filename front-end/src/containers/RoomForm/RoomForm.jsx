import React, {Component, Fragment} from "react";
import axios from "axios";
import "./RoomForm.css"

class RoomForm extends Component {

    state = {
        globalMessage: null,
        hotels: [],
        formData: {
            name: {
                value: "",
                isValid: true,
                message: null
            },
            hotelId: {
                value: "",
                isValid: true,
                message: null
            },
            numberOfBeds: {
                value: "",
                isValid: true,
                message: null
            },
            pricePerNight: {
                value: "",
                isValid: true,
                message: null
            },
            description: {
                value: "",
                isValid: true,
                message: null
            },
            imageUrl: {
                value: "",
                isValid: true,
                message: null
            }
        }
    };


    componentDidMount() {
        if (!localStorage.getItem("user")) {
            this.props.history.push("/login");
        }

        axios.get('/api/hotels/list')
            .then(response => {
                this.setState({
                    hotels: response.data
                })
            });

        if (this.props.match.params.roomId) {
            axios.get('/api/rooms/' + this.props.match.params.roomId)
                .then(response => {
                    const updatedRoomForm = {...this.state.formData};

                    for (let field in response.data) {
                        const updatedFormElement = {...updatedRoomForm[field]};
                        updatedFormElement.value = response.data[field];
                        updatedRoomForm[field] = updatedFormElement;

                    }
                    this.setState({
                        formData: updatedRoomForm
                    });
                })
        }

        if (this.props.match.params.hotelId) {
            let updatedRoomForm = {...this.state.formData};
            let hotelId = updatedRoomForm.hotelId;

            hotelId.value = this.props.match.params.hotelId;
            updatedRoomForm.hotelId = hotelId;

            this.setState({
                formData: updatedRoomForm
            });
        }
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.match.params.roomId && !this.props.match.params.roomId) {
            const updatedRoomForm = {...this.state.formData};
            for (let field in updatedRoomForm) {
                updatedRoomForm[field].value = "";
            }
            this.setState({
                formData: updatedRoomForm
            });
        }
    }

    formSubmit = event => {
        event.preventDefault();

        const data = {};
        for (let formElementIdentifier in this.state.formData) {
            data[formElementIdentifier] = this.state.formData[formElementIdentifier].value;
        }

        let url = "/api/rooms";
        let method = "post";
        // let path = '';

        if (this.props.match.params.roomId) {
            // path = "/rooms/" + this.props.match.params.id;
            url += "/" + this.props.match.params.roomId;
            method = "put";
        } else {
            // path = "/hotels/" + this.state.formData.hotelId.value;
        }

        axios({method: method, url: url, data: data})
            .then((response) => {
                this.props.history.push("/rooms/" + response.data);
            })
            .catch(error => {
                console.warn("error.response: ", error === null ? error.response : error);
                this.validationHandler(error);
            });
    };

    validationHandler = error => {
        const responseData = error.response.data;
        const updatedFormData = {...this.state.formData};
        if (responseData !== null) {
            if (responseData.globalError) {
                this.setState({globalMessage: responseData.globalError});
            }
            if (responseData.fieldErrors) {
                for (let fe of responseData.fieldErrors) {
                    updatedFormData[fe.field] = {
                        value: this.state.formData[fe.field].value,
                        isValid: false,
                        message: fe.message
                    }
                }
            }
            this.setState({formData: updatedFormData});
        } else {
            this.setState({globalMessage: error});
        }
    };

    inputChangeHandler = event => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            message: null
        };

        this.setState({formData: updatedFormData})
    };

    selectChangeHandler = (event) => {
        let updatedState = this.state;
        updatedState.formData.hotelId.value = event.target.value;
        updatedState.formData.hotelId.isValid = true;
        updatedState.formData.hotelId.message = null;
        this.setState(updatedState);

    };

    uploadWidget = () => {
        window.cloudinary.openUploadWidget({cloud_name: 'progmasters', upload_preset: 'ffrm3vqx', maxFiles: 1},
            (error, result) => {
                if (!error && result && result.event === "success") {
                    // console.log('Done! Here is the image info: ', result.info);
                    let updateFormData = {...this.state.formData};
                    let imageUrl = {...updateFormData.imageUrl}
                    imageUrl.value = result.info.secure_url
                    updateFormData.imageUrl = imageUrl;

                    this.setState({
                        formData: updateFormData
                    })
                } else if (error) {
                    console.warn("error.response: ", error);
                }
            });
    };

    showImage = () => {

        return (
            <div>
                {this.state.formData.imageUrl.value !== "" ?
                    <div>
                        <label><strong>Image:    </strong></label>
                        <img src={this.state.formData.imageUrl.value} className="room-image" alt="Room"/>
                    </div>
                    : null}

            </div>
        )
    };

    render() {

        return (
            <Fragment>
                <h2><strong>{this.props.match.params.roomId ? "Edit Room" : "Register New Room"}</strong></h2>
                <hr/>
                {this.state.globalMessage ?
                    <div className="alert alert-warning">{this.state.globalMessage}</div> : ""}
                <form onSubmit={this.formSubmit}>
                    <div className="form-group">
                        <label><strong>Room name:</strong></label>
                        <input
                            type="text"
                            className={this.state.formData.name.isValid ? "form-control" : "form-control is-invalid"}
                            name="name"
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.name.value}
                            placeholder="Enter room name"
                        />
                        <small className="form-text text-danger">{this.state.formData.name.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Hotel:</strong></label>
                        <select
                            name="hotelId"
                            value={this.state.formData.hotelId.value || ''}
                            onChange={this.selectChangeHandler}
                            className={this.state.formData.hotelId.isValid ? "form-control" : "form-control is-invalid"}
                        >
                            <option key="" value="" disabled>-- Please choose a hotel --</option>
                            {Object.entries(this.state.hotels).map(([key, value]) => {
                                return <option key={key} value={key}>{value}</option>
                            })}
                        </select>
                        <small className="form-text text-danger">{this.state.formData.hotelId.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Number of people:</strong></label>
                        <input
                            type="text"
                            className={this.state.formData.numberOfBeds.isValid ? "form-control" : "form-control is-invalid"}
                            name="numberOfBeds"
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.numberOfBeds.value}
                            placeholder="Enter the number of people that can sleep in the room"
                        />
                        <small className="form-text text-danger">{this.state.formData.numberOfBeds.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Price per night:</strong></label>
                        <input
                            type="text"
                            className={this.state.formData.pricePerNight.isValid ? "form-control" : "form-control is-invalid"}
                            name="pricePerNight"
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.pricePerNight.value}
                            placeholder="Enter the price per night"
                        />
                        <small className="form-text text-danger">{this.state.formData.pricePerNight.message}</small>
                    </div>
                    <div className="form-group">
                        <label><strong>Description:</strong></label>
                        <textarea
                            className={this.state.formData.description.isValid ? "form-control" : "form-control is-invalid"}
                            name="description"
                            onChange={this.inputChangeHandler}
                            placeholder="Enter room description"
                            rows="4"
                            value={this.state.formData.description.value}
                        />
                        <small className="form-text text-danger">{this.state.formData.description.message}</small>
                    </div>
                    <div className="form-group">
                        {this.showImage()}
                    </div>
                    <button type="button" onClick={this.uploadWidget.bind(this)} className="btn btn-primary button">
                        {this.state.formData.imageUrl.value === "" ? "Add an Image" : "Change Image"}
                    </button>
                    <button
                        type="submit"
                        className="btn btn-primary"
                    >{this.props.match.params.roomId ? "Save Changes" : "Register Room"}</button>
                </form>
            </Fragment>
        )
    }
}

export default RoomForm;