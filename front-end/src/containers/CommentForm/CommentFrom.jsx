import React, {Component} from 'react';
import StarRatingComponent from 'react-star-rating-component';
import axios from 'axios/index';
import './CommentForm.css'

class CommentFrom extends Component {

    initState = {
        commentForm: {
            author: {
                value: '',
                isValid: true,
                message: ''
            },
            text: {
                value: '',
                isValid: true,
                message: ''
            },
            rating: {
                value: 0,
                isValid: true,
                message: ''
            },
        }
    };

    componentDidMount() {
        const item = JSON.parse(localStorage.getItem("user"));
        const updatedcommentForm = {...this.state.commentForm};
        updatedcommentForm.author.value = item.name;
        this.setState({
            commentForm: updatedcommentForm
        })
    };


    state = this.initState;

    inputChangeHandler = (event) => {
        const updatedcommentForm = {...this.state.commentForm};
        const updatedFormElement = {...updatedcommentForm[event.target.name]};

        updatedFormElement.value = event.target.value;
        updatedFormElement.isValid = 'true';
        updatedFormElement.message = '';
        updatedcommentForm[event.target.name] = updatedFormElement;

        this.setState({commentForm: updatedcommentForm});
    };

    onStarClick(nextValue, prevValue, name) {
        const updatedcommentForm = {...this.state.commentForm};

        updatedcommentForm.rating.value = nextValue;
        updatedcommentForm.rating.message = '';
        updatedcommentForm.rating.isValid = 'true';

        this.setState({commentForm: updatedcommentForm});
    }

    validationHandler = error => {
        const statePatch = {...this.state.commentForm};
        for (let fieldError of error.fieldErrors) {
            statePatch[fieldError.field] = {
                value: this.state.commentForm[fieldError.field].value,
                isValid: false,
                message: fieldError.message
            }
        }
        this.setState({commentForm: statePatch});
    };

    formSubmit = event => {
        event.preventDefault();

        const formData = {};
        for (let formElementID in this.state.commentForm) {
            if (this.state.commentForm.hasOwnProperty(formElementID)) {
                formData[formElementID] = this.state.commentForm[formElementID].value;
            }
        }

        formData["hotelId"] = this.props.hotelId;

        axios.post('/api/comments', formData)

            .then(() => {
                this.props.getHotel();
                this.onStarClick(0)
                this.setState(this.initState)
                // this.props.history.push('/postDetails/' + this.props.match.params.id)
            })
            .catch(error => {
                this.validationHandler(error.response.data);
            })
    };

    render() {

        const ratingValue = ["", "Unacceptable", "Bad", "Average", "Good", "Very Good"];

        return (
            <div className="container">
                <form onSubmit={this.formSubmit}>
                    <br/>
                    <h4 className="text-left"><strong>Write a Review</strong></h4>
                    <div className="row">
                        <div className="col-sm-2 paddingTop">
                            <strong>Rate the Hotel:</strong>
                        </div>

                        <div className="starDesign">
                            <StarRatingComponent
                                name="rating"
                                starCount={5}
                                starColor='#EEC41C'
                                value={this.state.commentForm.rating.value}
                                onStarClick={this.onStarClick.bind(this)}
                            />
                        </div>
                        <div className="col-sm-2 text-left paddingTop">
                            {ratingValue[this.state.commentForm.rating.value]}
                        </div>
                        <div className="col-sm-6 text-left paddingTop">
                            <small className="text-danger">{this.state.commentForm.rating.message}</small>
                        </div>
                    </div>
                    <div className="form-group has-danger" align="center">
                        <input
                            className={this.state.commentForm.author.isValid ? "form-control" : "form-control is-invalid"}
                            name="author"
                            type="text"
                            placeholder={this.state.commentForm.author.value}
                            onChange={this.inputChangeHandler}
                            value={this.state.commentForm.author.value}
                            disabled
                        />
                        <small className="text-danger">{this.state.commentForm.author.message}</small>
                    </div>

                    <div className="form-group has-danger" align="center">
                        <textarea
                            className={this.state.commentForm.text.isValid ? "form-control" : "form-control is-invalid"}
                            name="text"
                            type="text"
                            rows="6"
                            placeholder="Review"
                            onChange={this.inputChangeHandler}
                            value={this.state.commentForm.text.value}/>
                        <small className="text-danger">{this.state.commentForm.text.message}</small>
                    </div>

                    <br/>
                    <div className="col-lg 4" align="right">
                        <button type="submit" className="btn btn-primary">Save Review</button>
                        {/*<button type="button" className="btn btn-primary button" onClick={this.props.handler}>Cancel</button>*/}
                    </div>
                </form>
            </div>
        )
    }
}


export default CommentFrom;

