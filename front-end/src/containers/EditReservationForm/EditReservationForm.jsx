import React, {Component} from "react";
import axios from "axios";


class EditReservationForm extends Component {

    state = {

        reservationFormData: {
            guestName: {
                value: '',
                isValid: true,
                message: ''
            },
            startDate: {
                value: '',
                isValid: true,
                message: ''
            },
            endDate: {
                value: '',
                isValid: true,
                message: ''
            },
            roomId: {
                value: '',
                isValid: true,
                message: ''
            },
            reservationId: {
                value: '',
                isValid: true,
                message: ''
            }
        }
    };


    componentDidMount() {
        axios.get('/api/rooms/reservations/' + this.props.match.params.id)
            .then(response => {
                const updatedReservationForm = {...this.state.reservationFormData};

                for (let field in response.data) {
                    const updatedFormElement = {...updatedReservationForm[field]};
                    updatedFormElement.value = response.data[field];
                    updatedReservationForm[field] = updatedFormElement;

                }
                this.setState({
                    reservationFormData: updatedReservationForm
                });

            });
    };


    formSubmit = event => {
        event.preventDefault();

        const item = JSON.parse(localStorage.getItem("user"));
        const role = item.role.substring(5);

        const data = {};
        for (let formElementIdentifier in this.state.reservationFormData) {
            data[formElementIdentifier] = this.state.reservationFormData[formElementIdentifier].value;
        }
        if (role === "ADMIN") {
            axios.put("/api/rooms/reservations/" + this.props.match.params.id, data)
                .then(() => {
                    this.props.history.push("/rooms/" + this.state.reservationFormData.roomId.value);
                })
                .catch(error => {
                    console.warn("error.response: ", error === null ? error.response : error);
                    this.validationHandler(error.response.data);
                });
        }
        else if(role === "USER"){
            axios.put("/api/rooms/myReservations/" + this.props.match.params.id, data)
                .then(() => {
                    this.props.history.push("/my-reservations");
                })
                .catch(error => {
                    console.warn("error.response: ", error === null ? error.response : error);
                    this.validationHandler(error.response.data);
                });
        }
    };


    inputChangeHandler = (event) => {
        const updatedReservationForm = {...this.state.reservationFormData};
        const updatedFormElement = {...updatedReservationForm[event.target.name]};

        updatedFormElement.value = event.target.value;
        updatedFormElement.isValid = 'true';
        updatedFormElement.message = '';
        updatedReservationForm[event.target.name] = updatedFormElement;

        this.setState({reservationFormData: updatedReservationForm});
    };

    validationHandler = (error) => {
        const updatedReservationForm = {...this.state.reservationFormData};

        for (let field in this.state.reservationFormData) {
            const updatedFormElement = {
                ...updatedReservationForm[field]
            };
            updatedFormElement.isValid = true;
            updatedFormElement.message = '';
            updatedReservationForm[field] = updatedFormElement;
        }

        for (let fieldError of error.fieldErrors) {
            const updatedFormElement = {
                ...updatedReservationForm[fieldError.field]
            };
            updatedFormElement.isValid = false;
            updatedFormElement.message = fieldError.message;
            updatedReservationForm[fieldError.field] = updatedFormElement;
        }
        this.setState({reservationFormData: updatedReservationForm});
    };


    render() {
        return (

            <div>
                <h2>Edit reservation</h2>
                <hr/>
                <br/>
                <form onSubmit={this.formSubmit}>
                    <div className="form-group">
                        <label>Guest name:</label>
                        <input type="text"
                               className={this.state.reservationFormData.guestName.isValid ? "form-control" : "form-control is-invalid"}
                               name="guestName"
                               value={this.state.reservationFormData.guestName.value}
                               onChange={this.inputChangeHandler}
                        />
                        <small
                            className="form-text text-danger">{this.state.reservationFormData.guestName.message}</small>
                    </div>

                    < div className="form-group">
                        <label>Check-in date: </label>
                        <input type="date"
                               name="startDate"
                               value={this.state.reservationFormData.startDate.value}
                               onChange={this.inputChangeHandler}
                        />
                        <small
                            className="form-text text-danger">{this.state.reservationFormData.startDate.message}</small>
                    </div>
                    <div>
                        <label>Check-out date: </label>
                        <input type="date"
                               name="endDate"
                               value={this.state.reservationFormData.endDate.value}
                               onChange={this.inputChangeHandler}
                        />
                        <small
                            className="form-text text-danger">{this.state.reservationFormData.endDate.message}</small>
                    </div>
                    <button className="btn btn-primary my-buttons" type="submit">Change reservation</button>
                </form>
            </div>
        )

    }
}

export default EditReservationForm