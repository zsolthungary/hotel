import React, {Component} from 'react';
import axios from 'axios';
import ReservationList from "../../components/ReservationList/ReservationList";
import ReservationForm from "../../components/ReservationForm/ReservationForm";
import './roomDetails.css';
import {Link} from "react-router-dom";
import CalendarListItem from "../../components/CalendarListItem/CalendarListItem";

class RoomDetails extends Component {


    state = {
        numberOfToday: 0,
        firstDayOfMonthOfYear: 0,
        currentMonth: 0,
        monthNumber: 0,
        nameOfMonth: "",
        month: [],
        reservations: [],
        room: {
            id: 0,
            hotelId: 0,
            hotelName: '',
            name: '',
            numberOfBeds: 0,
            pricePerNight: 0,
            description: '',
            imageUrl: ''
        },
        reservationFormData: {
            guestName: {
                value: '',
                isValid: true,
                message: ''
            },
            startDate: {
                value: '',
                isValid: true,
                message: ''
            },
            endDate: {
                value: '',
                isValid: true,
                message: ''
            },
            roomId: {
                value: 0,
                isValid: true,
                message: ''
            }
        },
    };


    goBack = () => {
        this.props.history.goBack();
    };

    editRoom = () => {
        this.props.history.push("/edit-room/" + this.props.match.params.id);
    };


    editReservation = (id) => {
        this.props.history.push('/edit-reservation/' + id);
    };

    componentDidMount() {
        axios.get('/api/rooms/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    room: response.data
                });

            });

        this.getReservations();

        if (this.state.reservationFormData.roomId.value !== this.props.match.params.id) {
            const updatedReservationForm = {...this.state.reservationFormData};
            const updatedRoomIdElement = {...updatedReservationForm.roomId};
            updatedRoomIdElement.value = this.props.match.params.id;

            updatedReservationForm.roomId = updatedRoomIdElement;
            this.setState({reservationFormData: updatedReservationForm});
        }
        this.getMonth();
    }


    getMonth = () => {
        axios.get('/api/calendar/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    numberOfToday: response.data.numberOfToday,
                    firstDayOfMonthOfYear: response.data.firstDayOfMonthOfYear,
                    currentMonth: response.data.currentMonth,
                    monthNumber: response.data.monthNumber,
                    nameOfMonth: response.data.nameOfMonth,
                    month: response.data.month
                });
            })
            .catch(error => {
                console.warn(error.response);
            });
    }

    checkUserRole = () => {
        let roleListener = false;
        if (localStorage.getItem("user")) {
            const item = JSON.parse(localStorage.getItem("user"));
            const role = item.role.substring(5);
            if (role === "USER") {
                roleListener = true;
            }
        }
        return roleListener;
    };

    checkAdminRole = () => {
        let roleListener = false;
        if (localStorage.getItem("user")) {
            const item = JSON.parse(localStorage.getItem("user"));
            const role = item.role.substring(5);
            if (role === "ADMIN") {
                roleListener = true;
            }
        }
        return roleListener;
    };

    inputChangeHandler = (event) => {
        const updatedReservationForm = {...this.state.reservationFormData};
        const updatedFormElement = {...updatedReservationForm[event.target.name]};

        updatedFormElement.value = event.target.value;
        updatedFormElement.isValid = 'true';
        updatedFormElement.message = '';
        updatedReservationForm[event.target.name] = updatedFormElement;

        this.setState({reservationFormData: updatedReservationForm});
    };


    getReservations = () => {
        axios.get('/api/rooms/' + this.props.match.params.id + '/reservations')

            .then(response => {
                this.setState({
                    reservations: response.data.reservations
                });
            });
    };

    getNextMonth = (event) => {
        event.preventDefault();
        let numberOfMonth = this.state.monthNumber;
        if (event.target.name === "next") {
            numberOfMonth++;
        }
        else if (event.target.name === "prev") {
            if (this.state.monthNumber > this.state.currentMonth) {
                numberOfMonth--;
            }
        }

        axios.get('/api/calendar/' + this.props.match.params.id + '/month/' + numberOfMonth)
            .then(response => {
                this.setState({
                    firstDayOfMonthOfYear: response.data.firstDayOfMonthOfYear,
                    monthNumber: response.data.monthNumber,
                    nameOfMonth: response.data.nameOfMonth,
                    month: response.data.month
                });
            })
            .catch(error => {
                console.warn(error.response);
            });
    }

    reservationSubmit = (event) => {
        event.preventDefault();

        const item = JSON.parse(localStorage.getItem("user"));
        const userName = item.name;

        const formData = {};
        for (let formElementId in this.state.reservationFormData) {
            if (this.state.reservationFormData.hasOwnProperty(formElementId)) {
                formData[formElementId] = this.state.reservationFormData[formElementId].value;
            }
        }

        axios({
            method: 'POST',
            url: 'api/rooms/' + this.props.id + '/reservations/' + userName,
            data: formData
        }).then((response) => {

            let updateReservationFormData = this.state.reservationFormData;
            updateReservationFormData.guestName.value = '';
            updateReservationFormData.guestName.isValid = true;
            updateReservationFormData.guestName.message = '';

            updateReservationFormData.startDate.value = '';
            updateReservationFormData.startDate.isValid = true;
            updateReservationFormData.startDate.message = '';

            updateReservationFormData.endDate.value = '';
            updateReservationFormData.endDate.isValid = true;
            updateReservationFormData.endDate.message = '';

            this.setState({reservationFormData: updateReservationFormData});

            this.getReservations();
            this.getMonth()
        }).catch(error => {
            console.warn('RoomDetails reservationSubmit error', error);
            console.warn('RoomDetails reservationSubmit error.response', error.response);
            this.validationHandler(error.response.data);
        });


    };

    calendarReservationSubmit = (event) => {
        event.preventDefault();
        const item = JSON.parse(localStorage.getItem("user"));
        const userName = item.name;

        const data = {};
        data["roomId"] = this.props.match.params.id;
        data["guestName"] = this.state.reservationFormData.guestName.value;
        data["firstDayOfSendedMonth"] = this.state.firstDayOfMonthOfYear;
        data["currentMonth"] = this.state.currentMonth;
        data["sendedMonth"] = this.state.month;

        axios.post("/api/calendar/reservation/" + userName, data)
            .then(() => {
                let updateReservationFormData = this.state.reservationFormData;
                updateReservationFormData.guestName.value = '';
                updateReservationFormData.guestName.isValid = true;
                updateReservationFormData.guestName.message = '';

                this.getReservations();
                this.getMonth();
            })
            .catch(error => {
                console.warn("error.response: ", error === null ? error.response : error);
                //this.validationHandler(error.response.data);
            });
    }

    validationHandler = (error) => {
        const updatedReservationForm = {...this.state.reservationFormData};

        for (let field in this.state.reservationFormData) {
            const updatedFormElement = {
                ...updatedReservationForm[field]
            };
            updatedFormElement.isValid = true;
            updatedFormElement.message = '';
            updatedReservationForm[field] = updatedFormElement;
        }

        for (let fieldError of error.fieldErrors) {
            const updatedFormElement = {
                ...updatedReservationForm[fieldError.field]
            };
            updatedFormElement.isValid = false;
            updatedFormElement.message = fieldError.message;
            updatedReservationForm[fieldError.field] = updatedFormElement;
        }
        this.setState({reservationFormData: updatedReservationForm});
    };

    deleteRoomHandler = (id) => {
        axios.delete('/api/rooms/' + id)
            .then(response => {
                this.props.history.push("/")
            })
            .catch(error => {
                console.log(error.response);
            });
    };


    deleteReservationHandler = (id) => {
        axios.delete('/api/rooms/reservations/' + id)

            .then(response => {
                this.getReservations();
                this.props.history.push('/rooms/' + this.props.match.params.id)
            })
            .catch(error => {
                console.warn(error.response);
            });

    };

    clickListener = (props, column) => {
        if (this.checkUserRole()) {
            let status = this.state.month[props.row][column][1];
            const valueOfChoosenDay = Number(this.state.month[props.row][column][0]) + Number(this.state.firstDayOfMonthOfYear) - 1;
            const updatedState = [...this.state.month];

            if (!(status.includes("R") || status.includes("N") || valueOfChoosenDay <= this.state.numberOfToday)) {
                updatedState[props.row][column][1] = "C";
                this.setState({
                    month: updatedState,
                })
            }
            if (status.includes("C")) {
                console.log("MONTH: ", this.state.month)
                console.log("FLAG")
                updatedState[props.row][column][1] = "F";
                this.setState({
                    month: updatedState,
                })
            }
        }
    };


    render() {
        let imgUrl;
        if (this.state.room.imageUrl === null || this.state.room.imageUrl.trim() === '') {
            imgUrl = '/images/noimage.png';
        } else {
            imgUrl = this.state.room.imageUrl;
        }

        let calendar;
        calendar = this.state.month.map((week, index) => (
                <CalendarListItem
                    weekDays={week}
                    key={index}
                    row={index}
                    listener={this.clickListener}
                />
            )
        );

        return (
            <div>
                <h2><strong>{this.state.room.name}</strong></h2>
                <hr/>
                <div className="row">
                    <div className="col-sm">
                        <img className="img-fluid" src={imgUrl} alt="Room"/>
                    </div>
                    <div className="col-sm text-left">
                        <dl className="row hotelName">
                            <dt className="col-sm-4">Hotel Name:</dt>
                            <Link to={"/hotels/" + this.state.room.hotelId}
                                  className="col-sm-8">{this.state.room.hotelName}</Link>
                        </dl>
                        <dl className="row">
                            <dt className="col-sm-4">Room Name:</dt>
                            <dd className="col-sm-8">{this.state.room.name} (id: {this.state.room.id})</dd>
                        </dl>
                        <dl className="row">
                            <dt className="col-sm-4">Number of people:</dt>
                            <dd className="col-sm-8">{this.state.room.numberOfBeds}</dd>
                        </dl>
                        <dl className="row">
                            <dt className="col-sm-4">Price per night:</dt>
                            <dd className="col-sm-8">{this.state.room.pricePerNight}</dd>
                        </dl>
                        <dl className="row">
                            <dt className="col-sm-4">Description:</dt>
                            <dd className="col-sm-8">{this.state.room.description}</dd>
                        </dl>
                        <div className="buttonContainer">
                            <button className="btn btn-outline-primary room-buttons" onClick={this.goBack}>
                                Go back to previous page
                            </button>
                            {this.checkAdminRole() ?
                                <button className="btn btn-outline-success room-buttons" onClick={this.editRoom}>
                                    Edit room details
                                </button> : ""}
                        </div>


                        {this.checkAdminRole() ?
                            <button type="button" className="btn btn-outline-danger room-buttons" data-toggle="modal"
                                    data-target="#exampleModal">
                                Delete room
                            </button> : ""}

                        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">Delete room</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        Are you sure you want to delete this room?
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            data-dismiss="modal"
                                            onClick={() => this.deleteRoomHandler(this.props.match.params.id)}
                                        >
                                            Yes
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-secondary"
                                            data-dismiss="modal"
                                        >
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <br/>

                <div className="calendar-box">
                    <div>
                        <h4 className="month">{this.state.nameOfMonth}</h4>
                    </div>
                    <table className="table table-striped table-bordered table-condensed table-hover calendar-table">
                        <thead>
                        <tr>
                            <th>Mon</th>
                            <th>Tue</th>
                            <th>Wed</th>
                            <th>Thu</th>
                            <th>Fri</th>
                            <th>Sat</th>
                            <th>Sun</th>
                        </tr>
                        </thead>
                        <tbody>
                        {calendar}
                        </tbody>
                    </table>
                </div>
                <div>
                    <button disabled={this.state.monthNumber === this.state.currentMonth}
                            className="btn btn-primary room-buttons" name="prev"
                            onClick={this.getNextMonth}>Previous
                        Month
                    </button>
                    <button className="btn btn-primary room-buttons" name="next" onClick={this.getNextMonth}>Next Month
                    </button>
                    {this.checkUserRole() ?
                        <button className="btn btn-primary room-buttons" name="reserve"
                                onClick={this.calendarReservationSubmit}>Reserve through calendar
                        </button> : ""}
                </div>
                <br/>

                {this.checkAdminRole() ? <ReservationList reservations={this.state.reservations}
                                                          reservationDelete={this.deleteReservationHandler}
                                                          reservationEdit={this.editReservation}
                /> : ""}

                {this.checkUserRole() ? <ReservationForm reservationFormData={this.state.reservationFormData}
                                                         inputChangeHandler={this.inputChangeHandler}
                                                         reservationSubmit={this.reservationSubmit}
                                                         validationHandler={this.validationHandler}/> : ""}


            </div>
        )
    }
}

export default RoomDetails;