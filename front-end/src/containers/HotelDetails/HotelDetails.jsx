import React, {Component} from 'react';
import axios from 'axios/index';

import RoomListItem from "../../components/RoomListItem/RoomListItem";
import CommentFrom from "../CommentForm/CommentFrom";
import CommentList from "../../components/CommentList/CommentList";
import StarRatingComponent from "react-star-rating-component";
import "./HotelDetails.css";
import ReactBnbGallery from 'react-bnb-gallery'
import HotelAction from "../../components/HotelAction/HotelAction";


class HotelDetails extends Component {

    state = {
        hotel: {
            id: 0,
            name: '',
            address: '',
            description: '',
            averageRating: 'Not Rated',
            imageUrls: [],
            rooms: [],
            comments: []
        },
        component: 'room',
        commentIsValid: false,
        images: [],
        imageIndex: 0,
        galleryOpened: false
    };

    getHotel = () => {
        axios.get('/api/hotels/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    hotel: response.data,
                    commentIsValid: false
                })
            })
            .then(data => {
                let images = [];

                this.state.hotel.imageUrls.forEach(image => {
                    images.push({photo: image})
                })

                this.setState({
                    images: images
                })
            })
            .catch(error => {
                console.warn(error)
            });
    };

    componentDidMount = () => {
        document.title = 'Hotel details';
        this.getHotel();
    };

    toggleGallery = (index) => {
        this.setState(prevState => ({
            imageIndex: index,
            galleryOpened: !prevState.galleryOpened
        }));
    };

    addComment = () => {
        return (
            <CommentFrom
                hotelId={this.state.hotel.id}
                getHotel={this.getHotel}
                // handler={this.handler}
            />
        )
    };

    commentButton = () => {
        if (localStorage.getItem("user")) {
            const item = JSON.parse(localStorage.getItem("user"));
            const role = item.role.substring(5);
            if (role === "USER") {
                return (
                    <div className="col-lg 4" align="right">
                        <button
                            className="btn btn-primary hotel-buttons"
                            type="submit"
                            onClick={() => this.setState({commentIsValid: true})}
                        >Add Review
                        </button>

                    </div>
                )
            }
        } else {
            return (
                <div className="text-left"><strong>Please login as a user to write a review!</strong></div>
            )
        }
    };


    render() {

        const roomList = this.state.hotel.rooms.map(room => {
            return <RoomListItem
                key={room.id}
                id={room.id}
                name={room.name}
                imageUrl={room.imageUrl}
                numberOfBeds={room.numberOfBeds}
                pricePerNight={room.pricePerNight}
                hotelName=""
                hotelId={room.hotelId}
            />
        });

        return (
            <div>

                <div className="row">
                    <div className="col-sm-7">
                        <h2 className="text-left"><strong>{this.state.hotel.name}</strong></h2>
                        <h6 className="text-muted text-left">{this.state.hotel.address}</h6>
                    </div>
                    <div className="col-sm text-right noPadding">
                        <StarRatingComponent
                            name="rating"
                            editing={false}
                            starCount={1}
                            starColor='#EEC41C'
                            value={this.state.hotel.averageRating !== 0 ? 1 : 0}
                        />
                    </div>
                    <div className="col-sm-1.5 text-left rating">
                        <h4>
                            <strong>{this.state.hotel.averageRating !== 0 ? this.state.hotel.averageRating + '/5' : 'Not Rated'}</strong>
                        </h4>
                    </div>
                </div>
                <hr/>
                <div className="row">
                    <div className="col-sm">
                        {this.state.hotel.imageUrls !== null ?
                            <img
                                src={this.state.hotel.imageUrls[0]}
                                className="img-fluid"
                                onClick={() => this.toggleGallery(0)}
                                alt="Hotel"
                            /> : null
                        }
                    </div>
                    <div className="col-sm-8 text-justify">
                        {this.state.hotel.description}
                    </div>

                </div>
                <br/>
                <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className="nav-link active label" data-toggle="tab"
                           href="#rooms">Rooms</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link label" data-toggle="tab"
                           href="#reviews">Reviews</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link label" data-toggle="tab"
                           href="#pictures">Pictures</a>
                    </li>
                    <li className="hotel-actions">
                        <div>
                            <HotelAction hotelId={this.state.hotel.id} history={this.props.history}/>
                        </div>
                    </li>
                </ul>
                <div id="myTabContent" className="tab-content">
                    <div className="tab-pane active fade show" id="rooms">
                        <br/>
                        {roomList}
                    </div>
                    <div className="tab-pane fade" id="reviews">
                        <br/>
                        <div>
                            {this.state.commentIsValid ? this.addComment() : this.commentButton()}
                        </div>
                        <br/>
                        <div>
                            <CommentList comments={this.state.hotel.comments}/>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="pictures">
                        <br/>
                        {this.state.hotel.imageUrls.map((image, index) => {
                            return (
                                image !== null && index !== 0 ?
                                    <img src={image} key={index} className="images"
                                         onClick={() => this.toggleGallery(index)}
                                         alt="Hotel"/> : null
                            )
                        })}

                        <ReactBnbGallery
                            show={this.state.galleryOpened}
                            activePhotoIndex={this.state.imageIndex}
                            photos={this.state.images}
                            onClose={this.toggleGallery}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default HotelDetails;