import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import "./messages.css"

class LoginForm extends Component {

    state = {
        userName: {
            value: '',
            isValid: true,
            message: ''
        },
        password: {
            value: '',
            isValid: true,
            message: ''
        }
    };

    componentDidMount() {
        document.title = 'Login form';
    }

    inputChangedHandler = (event) => {
        const target = event.target;
        const updatedFormElement = {...this.state[target.name]};

        updatedFormElement.value = event.target.value;
        updatedFormElement.isValid = true;
        updatedFormElement.message = null;

        let value;
        if (target.type === 'checkbox') {
            value = target.checked
                ? updatedFormElement.value.concat(target.value)
                : updatedFormElement.value.filter(val => val !== target.value);
        } else {
            value = target.value;
        }

        updatedFormElement.value = value;

        this.setState({[target.name]: updatedFormElement});
    };

    validationHandler = (error) => {
        const updatedState = {
            ...this.state
        };

        for (let field in this.state) {
            const updatedFormElement = {...updatedState[field]};
            updatedFormElement.isValid = true;
            updatedFormElement.message = '';
            updatedState[field] = updatedFormElement;
        }

        for (let fieldError of error.fieldErrors) {
            const updatedFormElement = {...updatedState[fieldError.field]};
            updatedFormElement.isValid = false;
            updatedFormElement.message = fieldError.message;
            updatedState[fieldError.field] = updatedFormElement;
        }

        this.setState(updatedState);
    }

    postDataHandler = (event) => {
        event.preventDefault();

        axios.get('/api/users/me', {
            auth: {
                username: this.state.userName.value,
                password: this.state.password.value
            }
        })
            .then(response => {
                localStorage.setItem('user', JSON.stringify(response.data));
                this.props.history.push('/');
            })
            .catch(error => {
                console.log(error.response);

                const errors = {
                    fieldErrors: [
                        {
                            field: 'userName',
                            message: 'Invalid username or password'
                        }
                    ]
                }
                this.validationHandler(errors);
            });
    }

    render() {
        return (
            <div>
                <h3><strong>Login form</strong></h3>
                <h6><strong>Please login or <Link to="/registration">Register</Link></strong></h6>
                <br/>
                <form onSubmit={this.postDataHandler}>
                    <div className={!this.state.userName.isValid ? "has-error" : null}>
                        <label className="control-label"><strong>Username:</strong></label>
                        <input
                            type="text"
                            name="userName"
                            value={this.state.userName.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Username"
                            className="form-control"
                        />
                        <span className="help-block error-messages">{this.state.userName.message}</span>
                    </div>

                    <div className={!this.state.password.isValid ? "has-error" : null}>
                        <label className="control-label"><strong>Password:</strong></label>
                        <input
                            type="password"
                            name="password"
                            value={this.state.password.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Password"
                            className="form-control"
                        />
                        <span className="help-block error-messages">{this.state.password.message}</span>
                    </div>
                    <br/>
                    <button type="submit" className="btn btn-primary">Login</button>
                </form>
            </div>
        )
    }
}

export default LoginForm;