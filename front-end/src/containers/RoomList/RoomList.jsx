import React, {Component, Fragment} from 'react';
import RoomListItem from '../../components/RoomListItem/RoomListItem';
import axios from 'axios';

class RoomList extends Component {

    state = {
        rooms: []
    };

    getRooms = () => {
        axios.get('/api/rooms')
            .then(response => {
                this.setState(
                    response.data
                );
            })
            .catch(error => {
                console.log(error.response);
            });
    }

    componentDidMount() {
        this.getRooms();
    }

    deleteRoomHandler = (id) => {
        axios.delete('/api/rooms/' + id)
            .then(response => {
                console.log(response);
                this.getRooms();
            })
            .catch(error => {
                console.log(error.response);
            });
    }


    render() {

        const rooms = this.state.rooms.map(room => {
            return (<RoomListItem
                    key={room.id}
                    id={room.id}
                    name={room.name}
                    imageUrl={room.imageUrl}
                    numberOfBeds={room.numberOfBeds}
                    pricePerNight={room.pricePerNight}
                    hotelName={room.hotel}
                    hotelId={room.hotelId}
                />
            )
        });

        return (
            <Fragment>
                <h2><strong>Rooms</strong></h2>
                <hr/>
                <div className="container">
                    {rooms}
                </div>
            </Fragment>
        )

    }
}


export default RoomList;