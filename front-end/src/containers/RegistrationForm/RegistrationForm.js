import React, {Component} from 'react';
import axios from 'axios';
import "./messages.css"

class RegistrationForm extends Component {

    state = {
        userName: {
            value: '',
            isValid: true,
            message: ''
        },
        email: {
            value: '',
            isValid: true,
            message: ''
        },
        password: {
            value: '',
            isValid: true,
            message: ''
        },
        retypedPassword: {
            value: '',
            isValid: true,
            message: ''
        }
    };

    // componentDidMount() {
    //     document.title = 'Registration form';
    // }

    componentDidMount() {
        // if (!localStorage.getItem("user")) {
        //     this.props.history.push("/login");
        // }
        document.title = 'Registration form';
        if (this.props.match.params.userId) {
            axios.get('/api/users/edit/' + this.props.match.params.userId)
                .then(response => {
                    const updatedRegistrationForm = {
                        ...this.state
                    };

                    for (let field in response.data) {
                        const updatedFormElement = {
                            ...updatedRegistrationForm[field]
                        };
                        updatedFormElement.value = response.data[field];
                        updatedRegistrationForm[field] = updatedFormElement;
                    }
                    this.setState(updatedRegistrationForm);
                    console.log("state", this.state);
                })
        }
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.match.params.userId && !this.props.match.params.userId) {
            const updatedRegistrationForm = {...this.state};

            for (let field in updatedRegistrationForm) {
                updatedRegistrationForm[field].value = "";
            }

            this.setState(updatedRegistrationForm);
        }
    }


    inputChangedHandler = (event) => {
        const target = event.target;
        const updatedFormElement = {...this.state[target.name]};

        updatedFormElement.value = event.target.value;
        updatedFormElement.isValid = true;
        updatedFormElement.message = null;

        let value;
        if (target.type === 'checkbox') {
            value = target.checked
                ? updatedFormElement.value.concat(target.value)
                : updatedFormElement.value.filter(val => val !== target.value);
        } else {
            value = target.value;
        }

        updatedFormElement.value = value;

        this.setState({[target.name]: updatedFormElement});
    };


    validationHandler = (error) => {
        const updatedState = {
            ...this.state
        };

        for (let field in this.state) {
            const updatedFormElement = {...updatedState[field]};
            updatedFormElement.isValid = true;
            updatedFormElement.message = '';
            updatedState[field] = updatedFormElement;
        }
        for (let fieldError of error.fieldErrors) {
            const updatedFormElement = {...updatedState[fieldError.field]};
            updatedFormElement.isValid = false;
            updatedFormElement.message = fieldError.message;
            updatedState[fieldError.field] = updatedFormElement;
        }

        this.setState(updatedState);
    };


    formSubmit = (event) => {
        event.preventDefault();

        const data = {};
        for (let formElementIdentifier in this.state) {
            data[formElementIdentifier] = this.state[formElementIdentifier].value;
        }

        axios.post('/api/users/register', data)
            .then(response => {
                console.log(response);
                this.props.history.push('/login');
            })
            .catch(error => {
                //console.warn("error.response: ", error === null ? error.response : error);
                console.log("ERROR: ", error);
                this.validationHandler(error.response.data);
            });
    };

    // formSubmit = event => {
    //     event.preventDefault();
    //
    //     const data = {};
    //     for (let formElementIdentifier in this.state) {
    //         data[formElementIdentifier] = this.state[formElementIdentifier].value;
    //     }
    //
    //     let url = "/api/users";
    //     let method = "post";
    //
    //     if (this.props.match.params.userId) {
    //
    //         url += "/edit-me";
    //         method = "put";
    //     }
    //     else {
    //         url += "/register";
    //         //method = "post";
    //     }
    //
    //     axios({method: method, url: url, data: data})
    //         .then((response) => {
    //             this.props.history.push("/");
    //         })
    //         .catch(error => {
    //             console.warn("error.response: ", error === null ? error.response : error);
    //             this.validationHandler(error);
    //         });
    // };


    render() {
        return (
            <div>
                <h3><strong>{this.props.match.params.userId ? "Edit your account" : "Registration form"}</strong></h3>
                <br/>
                <form onSubmit={this.formSubmit}>
                    <div className={!this.state.userName.isValid ? "has-error" : null}>
                        <label className="control-label"><strong>Username:</strong></label>
                        <input
                            type="text"
                            name="userName"
                            value={this.state.userName.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Username"
                            className="form-control"
                        />
                        <span
                            className="help-block error-messages">{this.state.userName.message}</span>
                    </div>

                    <div className={!this.state.email.isValid ? "has-error" : null}>
                        <label className="control-label"><strong>Email address:</strong></label>
                        <input
                            type="email"
                            name="email"
                            value={this.state.email.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Email"
                            className="form-control"
                        />
                        <span className="help-block error-messages">{this.state.email.message}</span>
                    </div>

                    <div className={!this.state.password.isValid ? "has-error" : null}>
                        <label
                            className="control-label"><strong>{this.props.match.params.userId ? "Old password:" : "Password:"}</strong></label>
                        <input
                            type="password"
                            name="password"
                            value={this.state.password.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Password"
                            className="form-control"
                        />
                        <span
                            className="help-block error-messages">{this.state.password.message}</span>
                    </div>


                    <div className={!this.state.password.isValid ? "has-error" : null}>
                        <label
                            className="control-label"><strong>{this.props.match.params.userId ? "New password:" : "Retype Password:"} </strong></label>
                        <input
                            type="password"
                            name="retypedPassword"
                            value={this.state.retypedPassword.value}
                            onChange={this.inputChangedHandler}
                            placeholder="Password"
                            className="form-control"
                        />
                        <span
                            className="help-block error-messages">{this.state.password.message}</span>
                    </div>

                    <br/>
                    <button type="submit"
                            className="btn btn-primary">{this.props.match.params.userId ? "Save Changes" : "Register"}</button>
                </form>
            </div>
        )
    }
}

export default RegistrationForm;