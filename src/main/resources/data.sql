use hotel;

INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (1, 'Lukasz', '2019-05-10 07:45:38', 5, 'Great location, nice and helpful staff, good breakfast. Comfortable rooms.
They even decorated the bed because of my wife birthdays.', 1, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (2, 'Aleksandra', '2019-05-10 07:46:00', 5, 'Big room with sea view, very good breakfast, polite and helpful staff.', 1, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (3, 'Karolyn', '2019-05-10 07:55:16', 4, 'Good stay and nice view from the room
 · Good location and helpful staff. The room was ok, but clean and comfortable, but considering that you are at a small and local island is perfect.
Very nice view from our room.
For the price is perfect.

 · You have to consider that you are not staying at a 5-star hotel, so expect a relax stay. Bathroom could be better and the water is kind of salty, but I don´t know if it is the only quality of water you can get at the island. Besides that, you are the beach so shouldn´t be a big problem.', 1, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (4, 'Oxana', '2019-05-10 07:56:00', 4, 'Quiet and pleasant stay
 · I liked my stay in Hibiscus Holidays very much. The building is new, the staff is nice and responsive. This is a family hotel, so all the family members were trying to do there best. The hired worker (Atit) was also very nice and doing his job very well. The island is very small and peaceful. No cars, no bikes, no dogs, no parties. That''s was exactly what I needed. Thanks to everybody!', 1, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (5, 'Jaroslaw', '2019-05-10 08:17:03', 5, 'Paradise
 · Rihiveli is one of the beautyfull island I have ever seen. Perfect beaches . Nice and helpful staff and teasty food. This wos my 4 time there and hope not the last one.', 2, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (6, 'Matteo', '2019-05-10 08:17:30', 4, 'simple and fantastic
 · location unica', 2, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (7, 'Kelly', '2019-05-10 08:18:06', 5, 'Dolphin dream came true
 · Swam with dolphins for the first time. Amazing.

 · I''ve had better coffee.', 2, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (8, 'Eldar', '2019-05-10 09:36:46', 5, 'Never been in place where the staff cleaning my room 3-4 times a day.
Overall, All the Island works are super friendly, The Resort itself is amazing and very valuable.', 3, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (9, 'Daniel', '2019-05-10 09:37:09', 5, 'The warm welcome of the hotel employees especially Ana from the lagoon restaurant. She’s so accommodating. The view from the watervilla is so astounding. It is indeed a piece of paradise on earth!', 3, null);
INSERT INTO hotel.comment (id, author, createdAt, rating, text, hotel_id, registerUser_id) VALUES (10, 'Yuri', '2019-05-10 10:15:22', 4, 'All was wonderful, especially thanks to Joker for attention, kitchen was very nice, room was clean.', 3, null);

INSERT INTO hotel.hotel (id, address, description, name) VALUES (1, 'Dhoores, 08070 Gulhi, Maldives', 'Hibiscus Holidays in Gulhi provides accommodations with a private beach area, a garden and a terrace. Among the facilities of this property are a restaurant, a 24-hour front desk and a tour desk, along with free WiFi. Staff on site can arrange airport transportation.

The rooms in the guesthouse are equipped with an electric tea pot. The rooms have a private bathroom with a hair dryer. All rooms at Hibiscus Holidays feature air conditioning and a desk.

A continental breakfast is available every morning at the accommodation.

Malé is a 13-minute walk from Hibiscus Holidays. Male Airport is 4.8 km from the property.', 'Hibiscus Holidays');
INSERT INTO hotel.hotel (id, address, description, name) VALUES (9, 'Raa Atoll North Province, Maldives, 21012 Raa Atoll, Maldives ', 'Dhigali Maldives is surrounded by a large house-reef, beyond the lagoon and is a 45-minute ride by sea plane from Male airport. The guests can enjoy a meal at one of the 7 dining options.

Guests can enjoy the Spa set in a soothing setting of a tropical lush garden, featuring an outdoor plunge pool surrounded by day beds within the natural greenery offering indigenous therapies and healing traditions.

Some rooms have a seating area for your convenience. You will find a coffee machine in the room. Every room has a private bathroom with a bath or shower and bidet, with bathrobes and slippers provided. For your comfort, you will find free toiletries.

A number of activities are offered in the area, such as snorkeling and windsurfing.', 'Dhigali Maldives');
INSERT INTO hotel.hotel (id, address, description, name) VALUES (2, 'South Male Atoll, 00960 Mahaanaelhihuraa, Maldives ', 'Rihiveli The Dream features sea-facing bungalows, each with its own private terrace overlooking the Indian Ocean. The island is located in Kaafu Male South Atoll, 43.5 km from the airport and is a 50-minute speed boat ride.

Charming bungalows with lagoon views, including terraces with sun loungers and hammocks. The tastefully decorated rooms are equipped with fans. Laundry service included.

The lagoon restaurant on stilts, offers a cuisine based on fresh produce. Buffet meal with themed dinners on the beach. Enjoy tropical cocktails at the sunset bar.

Guests can enjoy a variety of water sports including sailing, water-skiing and night snorkeling during their stay at Rihiveli The Dream. Other recreation facilities include volleyball, tennis and billiards. Rihiveli the Dream features a 5-star PADI scuba diving center offering all PADI courses with French speaking instructors.

This property also has one of the top-rated locations in Mahaanaelhihuraa! Guests are happier about it compared to other properties in the area.', 'Rihiveli The Dream, Maldives');
INSERT INTO hotel.hotel (id, address, description, name) VALUES (3, 'Lankanfinolhu, 08420 North Male Atoll, Maldives', 'Paradise Island Resort & Spa offers private villas and bungalows on an beautiful island in the Indian Ocean. The resort features an outdoor pool, 4 restaurants and a spa.

Guest units come with air conditioning, tiled flooring and a private terrace with serene ocean views. Amenities include satellite TV, a safe and a refrigerator. A tea/coffee maker is provided.

The Paradise Island Resort & Spa is a 20-minute speedboat ride from Malé International Airport. A mandatory transfer is provided by the resort, and charges are to be paid upon arrival.

The resort features a fitness center and tennis courts. Snorkeling and diving equipment is available. The resort provides a tour desk.

Dining choices include the Italian Ristorante al Tramonto, of local and international fame; The Lagoon, a quiet dining experience; Japanese restaurant Fukuya Teppanyaki; Seafood restaurant Farumathi; Athiri Bar; and the main restaurant Bageecha, which caters to many dietary requirements such as Jain meals. Hulhangu Bar and Dhaavani Coffee Shop offer beverages and food 24-hour.', 'Paradise Island Resort & Spa');
INSERT INTO hotel.hotel (id, address, description, name) VALUES (12, 'Pethényi', '', 'Heritance Aarah');

INSERT INTO hotel.registeruser (id, email, password, role, userName) VALUES (1, 'jakabzs@gmail.com', '$2a$10$tOLomCv3MXgS7n/QRiItw.j5Wn602I.CKtDf4.O8/DQnM.VQvFdmC', 'ROLE_ADMIN', 'StackMen');
INSERT INTO hotel.registeruser (id, email, password, role, userName) VALUES (2, 'jakabzs81@gmail.com', '$2a$10$u13E4T/nR8waQlC6aGIwausxrU.9GeIyDPTKSvKMqPzVjR0MtzRRy', 'ROLE_USER', 'Zsolt');

INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (2, 'This double room features a balcony, toaster and dining area.', 'https://r-ak.bstatic.com/images/hotel/max1024x768/163/163398349.jpg', 'Deluxe Double Room with Balcony', 2, 120, 1);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (3, 'This double room has a balcony, tile/marble floor and dining area.', 'https://q-ak.bstatic.com/images/hotel/max1024x768/163/163398477.jpg', 'Deluxe Double Room with Balcony and Sea View', 2, 190, 1);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (4, 'This double room features a balcony, toaster and electric kettle.', 'https://q-ak.bstatic.com/images/hotel/max1024x768/163/163398517.jpg', 'Deluxe Double Room with Side Sea View', 2, 170, 1);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (5, 'Breakfast, lunch & dinner included
1 twin bed', 'https://r-ak.bstatic.com/images/hotel/max1024x768/171/171446392.jpg', 'Double Room with Sea View', 2, 300, 2);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (6, 'Breakfast, lunch & dinner included
Select your bed (if available)
1 twin bed
1 queen bed', 'https://r-ak.bstatic.com/images/hotel/max1024x768/177/177873374.jpg', ' Single Room with Sea View', 2, 250, 2);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (7, 'Breakfast, lunch & dinner included', 'https://q-ak.bstatic.com/images/hotel/max1024x768/206/20615298.jpg', 'Deluxe Bungalow with Sea View', 3, 380, 2);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (8, ' Private bathroom, 1 queen bed', 'https://q-ak.bstatic.com/images/hotel/max1024x768/108/108138264.jpg', 'Garden Villa', 2, 300, 3);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (9, ' Private bathroom, 1 king bed', 'https://r-ak.bstatic.com/images/hotel/max1024x768/172/17262918.jpg', 'Water Villa', 2, 500, 3);
INSERT INTO hotel.room (id, description, imageUrl, name, numberOfBeds, pricePerNight, hotel_id) VALUES (10, 'The beach bungalows featuring interconnecting rooms, look out over sparkling blue-green waters. The bathroom has an open rainfall shower. An expansive covered outdoor veranda leads to the porcelain sands, where private sun loungers sit under native fronds. ', 'https://r-ak.bstatic.com/images/hotel/max1024x768/138/138462566.jpg', 'Bungalow Size', 2, 180, 9);