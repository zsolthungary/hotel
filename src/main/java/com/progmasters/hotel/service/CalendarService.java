package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Reservation;
import com.progmasters.hotel.dto.CalendarDetails;
import com.progmasters.hotel.dto.CalendarReservationDetails;
import com.progmasters.hotel.dto.ReservationForm;
import com.progmasters.hotel.repository.ReservationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class CalendarService {

    private ReservationRepository reservationRepository;
    private ReservationService reservationService;

    public CalendarService(ReservationRepository reservationRepository, ReservationService reservationService) {
        this.reservationRepository = reservationRepository;
        this.reservationService = reservationService;
    }


    public CalendarDetails getCalendarDetails(Long roomId, int numberOfMonth) {
        int monthNumber = 0;
        int firstDayOfMonthOfYear = 0;
        LocalDate foreignDate = LocalDate.now();
        LocalDate localDate = null;

        int numberOfToday = foreignDate.getDayOfYear();
        int currentMonth = foreignDate.getMonthValue();

        if (numberOfMonth == 0) {
            localDate = foreignDate;
            monthNumber = localDate.getMonthValue();
            LocalDate firstDayOfActualMonth = LocalDate.of(localDate.getYear(), Month.of(monthNumber), 1);
            firstDayOfMonthOfYear = firstDayOfActualMonth.getDayOfYear();
        } else {
            monthNumber = numberOfMonth;
            int year = foreignDate.getYear();
            localDate = LocalDate.of(year, Month.of(monthNumber), 1);
            firstDayOfMonthOfYear = localDate.getDayOfYear();
        }

        int dayOfYear = LocalDate.of(foreignDate.getYear(), Month.of(foreignDate.getMonthValue()), 1).getDayOfYear();
        int dayOfMonth = localDate.getDayOfMonth();

        List<Reservation> roomReservations = reservationRepository.getReservationsByRoomIdOrderByStartDateAsc(roomId);
        List<LocalDate> allDaysWhichAreReserved = new ArrayList<>();

        for (Reservation reservation : roomReservations) {
            LocalDate startDate = reservation.getStartDate();
            LocalDate endDate = reservation.getEndDate();
            List<LocalDate> reservedDates = startDate.datesUntil(endDate.plusDays(1)).collect(Collectors.toList());
            allDaysWhichAreReserved.addAll(reservedDates);
        }


        List<List<String[]>> weeks = new ArrayList<>();
        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        LocalDate currentDate = LocalDate.of(year, month, 1);
        String nameOfMonth = localDate.getMonth().toString();


        int startPoint = currentDate.getDayOfWeek().getValue() - 1;
        int monthInterval = currentDate.lengthOfMonth();
        int endPoint = startPoint + monthInterval;

        int fullLength = startPoint + monthInterval;

        if (fullLength % 7 != 0) {
            int numberOfWeeks = (fullLength / 7) + 1;
            fullLength = numberOfWeeks * 7;
        }


        for (int j = 0; j < fullLength / 7; j++) {
            List<String[]> daysOfWeek = new ArrayList<>();
            for (int k = 0; k < 7; k++) {
                int numberOfDay = (j * 7) + k;
                if (numberOfDay < startPoint || numberOfDay >= endPoint) {
                    String placeholder[] = new String[2];
                    placeholder[0] = "-";
                    placeholder[1] = "N";
                    daysOfWeek.add(placeholder);
                } else {
                    if (allDaysWhichAreReserved.contains(LocalDate.of(year, month, numberOfDay - startPoint + 1))) {
                        String stateOfDate[] = new String[2];
                        stateOfDate[0] = Integer.toString(numberOfDay - startPoint + 1);
                        stateOfDate[1] = "R";
                        daysOfWeek.add(stateOfDate);
                    } else {
                        String stateOfDate[] = new String[2];
                        stateOfDate[0] = Integer.toString(numberOfDay - startPoint + 1);
                        stateOfDate[1] = "F";
                        daysOfWeek.add(stateOfDate);
                    }
                }
            }
            weeks.add(daysOfWeek);
        }

        CalendarDetails calendarDetails = new CalendarDetails(currentMonth, monthNumber, firstDayOfMonthOfYear, numberOfToday, nameOfMonth, weeks);

        return calendarDetails;

    }

    public void createReservationThroughCalendar(CalendarReservationDetails calendarReservationDetails, String userName) {
        int firstDayOfMonthOfYear = calendarReservationDetails.getFirstDayOfSendedMonth();
        int dayOfYear = 0;
        int reservedDay = 0;
        String guestName = calendarReservationDetails.getGuestName();

        if(guestName.equals("")){
            guestName = userName;
        }



        LocalDate startDate = null;
        LocalDate endDate = null;
        int numberOfWeeks = calendarReservationDetails.getSendedMonth().size();

        String month[][] = new String[numberOfWeeks * 7][2];
        for (int i = 0; i < numberOfWeeks; i++) {
            for (int j = 0; j < 7; j++) {
                String days[] = calendarReservationDetails.getSendedMonth().get(i).get(j);
                month[(i * 7) + j][0] = days[0];
                month[(i * 7) + j][1] = days[1];
            }
        }

        boolean isItTheFirstChoosenDay = false;
        Year currentYear = Year.of(LocalDate.now().getYear());
        Map<LocalDate, LocalDate> reservedDates = new HashMap<>();

        for (int i = 0; i < month.length; i++) {

            if (month[i][1].equals("C") && !isItTheFirstChoosenDay) {
                isItTheFirstChoosenDay = true;

                dayOfYear = Integer.parseInt(month[i][0]);
                reservedDay = (firstDayOfMonthOfYear + dayOfYear) - 1;
                startDate = currentYear.atDay(reservedDay);
                reservedDates.put(startDate, endDate);
            }

            if ((!month[i][1].equals("C") && isItTheFirstChoosenDay) || (isItTheFirstChoosenDay && month[i].equals("-"))) {
                isItTheFirstChoosenDay = false;

                dayOfYear = Integer.parseInt(month[i - 1][0]);
                reservedDay = (firstDayOfMonthOfYear + dayOfYear) - 1;
                endDate = currentYear.atDay(reservedDay);
                reservedDates.put(startDate, endDate);

            } else if (isItTheFirstChoosenDay && i == month.length - 1) {
                isItTheFirstChoosenDay = false;

                dayOfYear = Integer.parseInt(month[i][0]);
                reservedDay = (firstDayOfMonthOfYear + dayOfYear) - 1;
                endDate = currentYear.atDay(reservedDay);
                reservedDates.put(startDate, endDate);
            }

        }

        for (Map.Entry<LocalDate, LocalDate> entry: reservedDates.entrySet()) {
            ReservationForm reservationForm = new ReservationForm();
            reservationForm.setGuestName(guestName);
            reservationForm.setStartDate(entry.getKey());
            reservationForm.setEndDate(entry.getValue());
            reservationForm.setRoomId(calendarReservationDetails.getRoomId());
            reservationService.createReservation(reservationForm, userName);
        }


    }
}