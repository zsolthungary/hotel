package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Image;
import com.progmasters.hotel.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ImageService {

    private ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public void save(Image image) {
        imageRepository.save(image);
    }

    public List<String> getImageUrlsByHotel(Hotel hotel) {
        List<String> imageUrls = new ArrayList<>();
        List<Image> images = imageRepository.findAllByHotelOrderByIdAsc(hotel);

        for (Image image : images) {
            imageUrls.add(image.getUrl());
        }

        return imageUrls;
    }

    public String getFirstImageByHotel(Hotel hotel) {
        List<String> imageUrls = imageRepository.findFirstImageByHotel(hotel);
        if (imageUrls.size() != 0) {
            return imageUrls.get(0);
        }

        return null;
    }

    public void updateImagesByHotel(List<String> newImageUrls, Hotel hotel) {
        List<String> oldImageUrls = getImageUrlsByHotel(hotel);

        for (String url : newImageUrls){
            if (!oldImageUrls.contains(url)) {
                save(new Image(url, hotel));
            }
        }
    }

    public void delete(Image image) {
        imageRepository.delete(image);
    }
}
