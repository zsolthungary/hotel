package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Comment;
import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.dto.CommentCommand;
import com.progmasters.hotel.dto.CommentDetails;
import com.progmasters.hotel.repository.CommentRepository;
import com.progmasters.hotel.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CommentService {

    private CommentRepository commentRepository;

    private HotelRepository hotelRepository;

    private RegisterService registerService;

    @Autowired
    public CommentService(CommentRepository commentRepository, HotelRepository hotelRepository, RegisterService registerService) {
        this.commentRepository = commentRepository;
        this.hotelRepository = hotelRepository;
        this.registerService = registerService;
    }

    public void saveComment(CommentCommand command) {
        Optional<Hotel> hotel = hotelRepository.findById(command.getHotelId());
        RegisterUser user = registerService.findUserByAuthor(command.getAuthor());

        if (hotel.isPresent() && user != null) {
            commentRepository.save(new Comment(command, hotel.get(), user));
        }
    }

    public List<CommentDetails> getCommentListByHotel(Hotel hotel) {
        List<Comment> comments = commentRepository.findAllByHotelOrderByCreatedAtAsc(hotel);
        List<CommentDetails> commentDetails = new ArrayList<>();

        for (Comment comment : comments) {
            commentDetails.add(new CommentDetails(comment));
        }

        return commentDetails;
    }

    public Double getAverageRatingByHotel(Hotel hotel) {
        return commentRepository.averageRating(hotel);
    }

    public void delete(Comment comment) {
        commentRepository.delete(comment);
    }
}
