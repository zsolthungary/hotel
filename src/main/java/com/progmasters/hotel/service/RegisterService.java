package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.domain.Role;
import com.progmasters.hotel.dto.RegisterDetails;
import com.progmasters.hotel.dto.RegisterUserDetails;
import com.progmasters.hotel.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RegisterService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    public RegisterService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public void register(RegisterDetails registerDetails) {
        RegisterUser registerUser = new RegisterUser();

        registerUser.setUserName(registerDetails.getUserName());
        registerUser.setEmail(registerDetails.getEmail());


        String encodedPassword = passwordEncoder.encode(registerDetails.getPassword());
        registerUser.setPassword(encodedPassword);

        registerUser.setRole(Role.ROLE_USER);

        userRepository.save(registerUser);
    }

    public RegisterUserDetails getUserDetails(Long id) {
        RegisterUserDetails registerUserDetails = new RegisterUserDetails();
        Optional<RegisterUser> registerUser = userRepository.findById(id);

        if (registerUser.isPresent()) {
            registerUserDetails.setUserId(registerUser.get().getId());
            registerUserDetails.setUserName(registerUser.get().getUserName());
            registerUserDetails.setEmail(registerUser.get().getEmail());
            registerUserDetails.setPassword(registerUser.get().getPassword());
        } else {
            throw new IllegalArgumentException("There is no User for this id: " + id);
        }

        return registerUserDetails;
    }

    public RegisterUser findUserByAuthor(String author) {
        return userRepository.findByUserName(author);
    }

    public RegisterUser updateRegisterUser(RegisterUserDetails registerUserDetails, Long id) {
        Optional<RegisterUser> optionalRegisterUser = userRepository.findById(id);

        if (optionalRegisterUser.isPresent()) {
            RegisterUser registerUser = optionalRegisterUser.get();
            updateValues(registerUserDetails, registerUser);
            return registerUser;
        }

        return null;
    }

    private void updateValues(RegisterUserDetails registerUserDetails, RegisterUser registerUser) {
        registerUser.setUserName(registerUserDetails.getUserName());
        registerUser.setEmail(registerUserDetails.getEmail());

        registerUser.setRole(Role.ROLE_USER);

        userRepository.save(registerUser);
    }
}
