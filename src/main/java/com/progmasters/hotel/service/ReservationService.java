package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.domain.Reservation;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.*;
import com.progmasters.hotel.repository.ReservationRepository;
import com.progmasters.hotel.repository.RoomRepository;
import com.progmasters.hotel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReservationService {

    private ReservationRepository reservationRepository;
    private RoomRepository roomRepository;
    private UserRepository userRepository;
    private EmailSenderService emailSenderService;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, RoomRepository roomRepository, UserRepository userRepository, EmailSenderService emailSenderService) {
        this.reservationRepository = reservationRepository;
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.emailSenderService = emailSenderService;
    }

    public ReservationList getReservationsByRoomId(Long roomId) {
        List<Reservation> reservations = reservationRepository.getReservationsByRoomIdOrderByStartDateAsc(roomId);
        List<ReservationListItem> reservationListItems = reservations.stream()
                .map(ReservationListItem::new).collect(Collectors.toList());
        return new ReservationList(reservationListItems);
    }

    public void createReservation(ReservationForm reservationForm, String userName) {
        Optional<Room> room = roomRepository.findById(reservationForm.getRoomId());
        RegisterUser registerUser = userRepository.findByUserName(userName);


        if (room.isPresent()) {
            Room currentRoom = room.get();
            reservationRepository.save(new Reservation(reservationForm, currentRoom, registerUser));
            sendMessage(reservationForm.getStartDate(), reservationForm.getEndDate(), registerUser, currentRoom, "POST");
        }
    }

    public boolean deleteReservation(Long id) {
        Optional<Reservation> reservation = reservationRepository.findById(id);

        boolean result = false;
        if (reservation.isPresent()) {
            reservationRepository.delete(reservation.get());

            Reservation currentReservation = reservation.get();

            Room currentRoom = currentReservation.getRoom();
            RegisterUser currentUser = currentReservation.getRegisterUser();
            LocalDate startDate = currentReservation.getStartDate();
            LocalDate endDate = currentReservation.getEndDate();

            sendMessage(startDate, endDate, currentUser, currentRoom, "DELETE");

            result = true;
        }
        return result;
    }

    public Reservation updateReservation(EditReservationDetails editReservationDetails, Long id) {
        Reservation reservation = reservationRepository.getOne(id);
        if (reservation != null) {
            updateValues(editReservationDetails, reservation);
        }

        return reservation;
    }

    private void updateValues(EditReservationDetails editReservationDetails, Reservation reservation) {
        reservation.setGuestName(editReservationDetails.getGuestName());
        reservation.setStartDate(editReservationDetails.getStartDate());
        reservation.setEndDate(editReservationDetails.getEndDate());

        LocalDate startDate = editReservationDetails.getStartDate();
        LocalDate endDate = editReservationDetails.getEndDate();

        sendMessage(startDate, endDate, reservation.getRegisterUser(), reservation.getRoom(), "PUT");

    }


    public Reservation getReservation(Long id) {
        Optional<Reservation> reservation = reservationRepository.findById(id);
        Reservation currentReservation = null;
        if (reservation.isPresent()) {
            currentReservation = reservation.get();
        }
        return currentReservation;
    }

    public List<MyReservationsListItem> getMyReservations(String userName) {
        List<Reservation> reservations = new ArrayList<>();
        List<MyReservationsListItem> myReservationsListItems = new ArrayList<>();
        RegisterUser registerUser = userRepository.findByUserName(userName);

        if (registerUser != null) {
            reservations = reservationRepository.findAllByRegisterUser(registerUser);
        }

        for (Reservation currentReservation : reservations) {
            myReservationsListItems.add(new MyReservationsListItem(currentReservation));
        }

        return myReservationsListItems;
    }

    public void sendMessage(LocalDate firstDate, LocalDate secondDate, RegisterUser registerUser, Room room, String method) {

        final String LINE_SEPARATOR = System.lineSeparator();

        String userName = registerUser.getUserName();
        String hotelName = room.getHotel().getName();
        String roomName = room.getName();
        String startDate = firstDate.toString();
        String endDate = secondDate.toString();
        int numberOfReservedDays = (secondDate.getDayOfYear() - firstDate.getDayOfYear()) + 1;
        int priceOfHoliday = numberOfReservedDays * room.getPricePerNight();


        StringBuilder details = new StringBuilder();
        details.append("Room: " + roomName + LINE_SEPARATOR);
        details.append("Date of stay: " + startDate + " - " + endDate + LINE_SEPARATOR);
        details.append("Number of reserved days: " + numberOfReservedDays + LINE_SEPARATOR);
        details.append("Full price: " + priceOfHoliday + LINE_SEPARATOR + LINE_SEPARATOR);
        details.append("Best wishes, " + LINE_SEPARATOR);
        details.append(hotelName);

        StringBuilder messageBody = new StringBuilder();
        messageBody.append("Dear " + userName + "!" + LINE_SEPARATOR + LINE_SEPARATOR);

        if (method.equals("POST")) {
            messageBody.append("Thank you for your reservation!" + LINE_SEPARATOR + LINE_SEPARATOR);
            messageBody.append("Please find your reservation details as following:" + LINE_SEPARATOR);
            messageBody.append(details);

        }
        else if(method.equals("PUT")){
            messageBody.append("Your reservation has been changed. New reservation is as follows: " + LINE_SEPARATOR);
            messageBody.append(details);
        }
        else if(method.equals("DELETE")){
            messageBody.append("Your below reservation has been deleted!" + LINE_SEPARATOR);
            messageBody.append(details);
        }


        emailSenderService.sendMessage("Reservation", messageBody.toString(), registerUser.getEmail());
    }

    public List<Object[]> getReservationDatesByRoomId(Long id) {
        return reservationRepository.findDatesByUserId(id);
    }

    public List<Reservation> getReservationsBtRoomId(Long id) {
        return reservationRepository.getAllByRoomId(id);
    }
}
