package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Reservation;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.*;
import com.progmasters.hotel.repository.HotelRepository;
import com.progmasters.hotel.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoomService {

    private RoomRepository roomRepository;
    private HotelRepository hotelRepository;
    private ReservationService reservationService;

    @Autowired
    public RoomService(RoomRepository roomRepository, HotelRepository hotelRepository, ReservationService reservationService) {
        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
        this.reservationService = reservationService;
    }

    public RoomList getRoomList() {
        List<Room> rooms = roomRepository.findAll();

        return new RoomList(createRoomListItems(rooms));
    }

    public List<RoomListItem> getRoomListByHotelOrderedByPrice(Hotel hotel) {
        List<Room> rooms = roomRepository.findAllByHotelOrderByPricePerNightAsc(hotel);

        return createRoomListItems(rooms);
    }

    public List<Long> getRoomListIdByHotel(Long hotelId) {
        return roomRepository.findAllIdByHotelId(hotelId);
    }

    private List<RoomListItem> createRoomListItems(List<Room> rooms) {
        List<RoomListItem> roomListItems = new ArrayList<>();

        for (Room room : rooms) {
            RoomListItem item = new RoomListItem();
            item.setId(room.getId());
            item.setName(room.getName());
            item.setNumberOfBeds(room.getNumberOfBeds());
            item.setPricePerNight(room.getPricePerNight());
            item.setImageUrl(room.getImageUrl());
            item.setHotel(room.getHotel().getName());
            item.setHotelId(room.getHotel().getId());

            roomListItems.add(item);
        }

        return roomListItems;
    }

    public RoomDetails getRoomDetails(Long roomId) {
        RoomDetails roomDetails = new RoomDetails();

        Optional<Room> room = roomRepository.findById(roomId);
        if (room.isPresent()) {
            roomDetails.setId(room.get().getId());
            roomDetails.setHotelName(room.get().getHotel().getName());
            roomDetails.setHotelId(room.get().getHotel().getId());
            roomDetails.setName(room.get().getName());
            roomDetails.setNumberOfBeds(room.get().getNumberOfBeds());
            roomDetails.setPricePerNight(room.get().getPricePerNight());
            roomDetails.setDescription(room.get().getDescription());
            roomDetails.setImageUrl(room.get().getImageUrl());
        } else {
            throw new IllegalArgumentException("There is no Room for this id: " + roomId);
        }

        return roomDetails;
    }


    public Long createRoom(RoomForm roomForm) {
        Optional<Hotel> hotel = hotelRepository.findById(roomForm.getHotelId());

        if (hotel.isPresent()) {
            Room room = roomRepository.save(new Room(roomForm, hotel.get()));

            return room.getId();
        } else {
            throw new IllegalArgumentException("There is no Hotel for this id: " + roomForm.getHotelId());
        }
    }


    public boolean deleteRoom(Long id) {
        Optional<Room> room = roomRepository.findById(id);

        boolean result = false;
        if (room.isPresent()) {
            if (room.get().getReservations() != null) {
                for (Reservation reservation : room.get().getReservations()) {
                    reservationService.deleteReservation(reservation.getId());
                }
            }

            roomRepository.delete(room.get());
            result = true;
        }
        return result;
    }

    public Room updateRoom(RoomForm roomForm, Long id) {
        Optional<Room> optionalRoom = roomRepository.findById(id);
        Optional<Hotel> optionalHotel = hotelRepository.findById(roomForm.getHotelId());

        if (optionalRoom.isPresent() && optionalHotel.isPresent()) {
            Room room = optionalRoom.get();
            Hotel hotel = optionalHotel.get();
            updateValues(roomForm, room, hotel);
            return room;
        }

        return null;
    }

    private void updateValues(RoomForm roomForm, Room room, Hotel hotel) {
        room.setName(roomForm.getName());
        room.setNumberOfBeds(roomForm.getNumberOfBeds());
        room.setPricePerNight(roomForm.getPricePerNight());
        room.setDescription(roomForm.getDescription());
        room.setImageUrl(roomForm.getImageUrl());
        room.setHotel(hotel);

        roomRepository.save(room);
    }

    public Integer getRoomNumber(Hotel hotel) {
        return roomRepository.getRoomNumberByHotel(hotel);
    }

}
