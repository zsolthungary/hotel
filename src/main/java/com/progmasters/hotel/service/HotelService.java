package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.*;
import com.progmasters.hotel.dto.*;
import com.progmasters.hotel.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class HotelService {

    private HotelRepository hotelRepository;
    private RoomService roomService;
    private CommentService commentService;
    private ImageService imageService;
    private ReservationService reservationService;

    @Autowired
    public HotelService(HotelRepository hotelRepository, RoomService roomService, CommentService commentService, ImageService imageService, ReservationService reservationService) {
        this.hotelRepository = hotelRepository;
        this.roomService = roomService;
        this.commentService = commentService;
        this.imageService = imageService;
        this.reservationService = reservationService;
    }

    public Hotel createHotel(HotelForm hotelForm) {
        Hotel hotel = hotelRepository.save(new Hotel(hotelForm));

        if (hotelForm.getImageUrls() != null) {
            for (String image : hotelForm.getImageUrls()) {
                imageService.save(new Image(image, hotel));
            }
        }

        return hotel;
    }

    public HotelDetails getHotelDetails(Long hotelId) {
        HotelDetails hotelDetails = new HotelDetails();
        Optional<Hotel> hotel = hotelRepository.findById(hotelId);

        if (hotel.isPresent()) {
            List<CommentDetails> commentList = commentService.getCommentListByHotel(hotel.get());
            List<RoomListItem> roomList = roomService.getRoomListByHotelOrderedByPrice(hotel.get());
            List<String> imageUrls = imageService.getImageUrlsByHotel(hotel.get());

            hotelDetails.setId(hotel.get().getId());
            hotelDetails.setName(hotel.get().getName());
            hotelDetails.setAddress(hotel.get().getAddress());
            hotelDetails.setDescription(hotel.get().getDescription());
            hotelDetails.setImageUrls(imageUrls);
            hotelDetails.setRooms(roomList);
            hotelDetails.setComments(commentList);
            hotelDetails.setAverageRating(countAverageRating(commentList));
        } else {
            throw new IllegalArgumentException("There is no Hotel for this id: " + hotelId);
        }

        return hotelDetails;
    }

    private Double countAverageRating(List<CommentDetails> commentList) {
        Integer sumOfRatings = 0;

        for (CommentDetails commentDetails : commentList) {
            sumOfRatings += commentDetails.getRating();
        }

        return Math.round((double) sumOfRatings / commentList.size() * 10) / 10.0;
    }

    public Map<Long, String> getHotelIdAndNameList() {
        List<Hotel> hotels = hotelRepository.findAll();
        Map<Long, String> hotelList = new HashMap<>();

        for (Hotel hotel : hotels) {
            hotelList.put(hotel.getId(), hotel.getName());
        }

        return hotelList;
    }

    public HotelList getHotelList(String searchData, LocalDate from, LocalDate to) {
        List<Hotel> hotels;

        if (searchData == null) {
            hotels = hotelRepository.findAll();
        } else {
            hotels = hotelRepository.findAllBySearch(searchData);
        }

        List<HotelListItem> hotelListItems = createHotelListItems(hotels);

        if (hotels != null && from != null && to != null) {
            for (HotelListItem hotelListItem : hotelListItems) {
                if (!hasEmptyRoom(hotelListItem, from, to)) {
                    hotelListItem.setHasEmptyRoom(false);
                }
            }
        }

        return new HotelList(hotelListItems);
    }

    private boolean hasEmptyRoom(HotelListItem hotelListItem, LocalDate from, LocalDate to) {
        List<Long> roomIdsByHotel = roomService.getRoomListIdByHotel(hotelListItem.getId());
        boolean result = false;

        if (roomIdsByHotel != null) {
            for (Long id : roomIdsByHotel) {
                List<Object[]> reservations = reservationService.getReservationDatesByRoomId(id);

                if (reservations != null) {
                    int i = 0;

                    for (Object[] obj : reservations) {
                        System.out.println(obj);
                        LocalDate startDate = (LocalDate) obj[0];
                        LocalDate endDate = (LocalDate) obj[1];

                        if (to.isBefore(startDate.plusDays(1)) || from.isAfter(endDate.minusDays(1))) {
                            i++;
                        } else {
                            break;
                        }
                    }
                    if (i == reservations.size()) {
                        result = true;
                        break;
                    }
                } else {
                    result = true;
                }
            }
        }

        return result;
    }

    private List<HotelListItem> createHotelListItems(List<Hotel> hotels) {
        List<HotelListItem> hotelListItems = new ArrayList<>();

        for (Hotel hotel : hotels) {
            HotelListItem item = new HotelListItem();
            Double averageRating = 0.0;

            if (commentService.getAverageRatingByHotel(hotel) != null) {
                averageRating = Math.round(commentService.getAverageRatingByHotel(hotel) * 10) / 10.0;
            }

            item.setId(hotel.getId());
            item.setName(hotel.getName());
            item.setAddress(hotel.getAddress());
            item.setImageUrl(imageService.getFirstImageByHotel(hotel));
            item.setNumberOfRooms(roomService.getRoomNumber(hotel));
            item.setAverageRating(averageRating);

            hotelListItems.add(item);
        }

        return hotelListItems;
    }

    public boolean deleteHotel(Long id) {
        Optional<Hotel> hotel = hotelRepository.findById(id);

        boolean result = false;
        if (hotel.isPresent()) {
            if (hotel.get().getImageUrls() != null) {
                for (Image image : hotel.get().getImageUrls()) {
                    imageService.delete(image);
                }
            }

            if (hotel.get().getRooms() != null) {
                for (Room room : hotel.get().getRooms()) {
                    roomService.deleteRoom(room.getId());
                }
            }

            if (hotel.get().getComments() != null) {
                for (Comment comment : hotel.get().getComments()) {
                    commentService.delete(comment);
                }
            }

            hotelRepository.delete(hotel.get());
            result = true;
        }

        return result;
    }


    public Hotel updateHotel(HotelForm hotelForm, Long id) {
        Optional<Hotel> optionalHotel = hotelRepository.findById(id);

        if (optionalHotel.isPresent()) {
            Hotel hotel = optionalHotel.get();
            updateValues(hotelForm, hotel);
            return hotel;
        }

        return null;
    }

    private void updateValues(HotelForm hotelForm, Hotel hotel) {
        hotel.setName(hotelForm.getName());
        hotel.setAddress(hotelForm.getAddress());
        hotel.setDescription(hotelForm.getDescription());

        hotelRepository.save(hotel);

        imageService.updateImagesByHotel(hotelForm.getImageUrls(), hotel);
    }

}
