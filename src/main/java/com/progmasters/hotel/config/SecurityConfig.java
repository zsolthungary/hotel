/*
 * Copyright © Progmasters (QTC Kft.), 2016-2019.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.hotel.config;

import com.progmasters.hotel.security.LogoutSuccessHandler;
import com.progmasters.hotel.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;




    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/users/**").permitAll()
                .antMatchers("/api/calendar/**").permitAll()
                .antMatchers(HttpMethod.GET,"/api/hotels").permitAll()
                .antMatchers(HttpMethod.GET,"/api/hotels/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rooms").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rooms/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rooms/{id}/reservations").permitAll()
                .antMatchers(HttpMethod.POST, "/api/rooms/{id}/reservations/{userName}").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/api/rooms/myReservations/**").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/api/rooms/reservations/{reservationId}").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/rooms/reservations/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/rooms").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/rooms/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/rooms/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/hotels").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/comments").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/api/calendar/reservation/{userName}").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/api/calendar/reservation/{userName}").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and().logout().logoutSuccessHandler(new LogoutSuccessHandler()).deleteCookies("JSESSIONID")
                .and().httpBasic().authenticationEntryPoint(restAuthenticationEntryPoint);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000", "http://stackmen.progmasters.hu/", "http://3.17.67.212"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
