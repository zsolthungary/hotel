package com.progmasters.hotel.validation;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.dto.RegisterUserDetails;
import com.progmasters.hotel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegisterUserDetailsValidator implements Validator {

    private UserRepository userRepository;

    @Autowired
    public RegisterUserDetailsValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterUserDetails.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        RegisterUserDetails registerUserDetails = (RegisterUserDetails) obj;
        RegisterUser registerUser = userRepository.findByUserName(registerUserDetails.getUserName());
        String password = registerUserDetails.getPassword();
        String retypedPassword = registerUserDetails.getRetypedPassword();

        if (registerUser != null) {
            errors.rejectValue("userName", "registration.userNameAlreadyExists");
        }
        if (!password.equals(retypedPassword)) {
            errors.rejectValue("password", "registration.passwordsDoNotMatch");
        }
        if (userRepository.findByEmail(registerUserDetails.getEmail()) != null) {
            errors.rejectValue("email", "registration.emailAlreadyExists");
        }

    }


}
