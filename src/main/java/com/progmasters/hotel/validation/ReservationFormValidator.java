package com.progmasters.hotel.validation;

import com.progmasters.hotel.domain.Reservation;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.ReservationForm;
import com.progmasters.hotel.repository.ReservationRepository;
import com.progmasters.hotel.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.Optional;

@Component
public class ReservationFormValidator implements Validator {

    private RoomRepository roomRepository;
    private ReservationRepository reservationRepository;

    @Autowired
    public ReservationFormValidator(RoomRepository roomRepository, ReservationRepository reservationRepository) {
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return ReservationForm.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ReservationForm form = (ReservationForm) obj;
        Optional<Room> room = roomRepository.findById(form.getRoomId());
        if (!room.isPresent()) {
            errors.rejectValue("roomId", "reservation.roomNotExists");
        }

        List<Reservation> reservationsByStartDateAsc = reservationRepository.getReservationsByRoomIdOrderByStartDateAsc(form.getRoomId());


        if (form.getStartDate() != null && form.getEndDate() != null) {

            if (form.getEndDate().isBefore(form.getStartDate())) {
                errors.rejectValue("endDate", "reservation.endDateBeforeStartDate");
            }

            for (Reservation reservation : reservationsByStartDateAsc) {
                if (
                        form.getStartDate().isEqual(reservation.getStartDate())
                                || (form.getStartDate().isAfter(reservation.getStartDate()) && form.getStartDate().isBefore(reservation.getEndDate()))
                                || (form.getEndDate().isAfter(reservation.getStartDate()) && form.getEndDate().isBefore(reservation.getEndDate()))
                ) {
                    errors.rejectValue("endDate", "reservation.priodIsAlreadyReserved");
                }
            }
        }
    }
}
