package com.progmasters.hotel.validation;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.dto.RegisterDetails;
import com.progmasters.hotel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class RegistrationFormValidator implements Validator {

    private UserRepository userRepository;

    @Autowired
    public RegistrationFormValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterDetails.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterDetails registerDetails = (RegisterDetails) o;
        RegisterUser registerUser = userRepository.findByUserName(registerDetails.getUserName());


        String password = registerDetails.getPassword();
        String retypedPassword = registerDetails.getRetypedPassword();

        if (registerUser != null) {
            errors.rejectValue("userName", "registration.userNameAlreadyExists");
        }
        if (!password.equals(retypedPassword)) {
            errors.rejectValue("password", "registration.passwordsDoNotMatch");
        }
        if (userRepository.findByEmail(registerDetails.getEmail()) != null) {
            errors.rejectValue("email", "registration.emailAlreadyExists");
        }

    }

}
