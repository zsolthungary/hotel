package com.progmasters.hotel.domain;

import com.progmasters.hotel.dto.CommentCommand;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String author;

    @Column(columnDefinition="text")
    private String text;

    private LocalDateTime createdAt;

    private Integer rating;

    @ManyToOne
    private Hotel hotel;

    @ManyToOne
    private RegisterUser registerUser;

    public Comment() {
    }

    public Comment(CommentCommand command, Hotel hotel, RegisterUser registerUser) {
        this.author = command.getAuthor();
        this.text = command.getText();
        this.createdAt = LocalDateTime.now();
        this.rating = command.getRating();
        this.hotel = hotel;
        this.registerUser = registerUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public RegisterUser getRegisterUser() {
        return registerUser;
    }

    public void setRegisterUser(RegisterUser registerUser) {
        this.registerUser = registerUser;
    }
}
