package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Reservation;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class EditReservationDetails {

    private Long roomId;

    private Long reservationId;

    @NotNull(message = "{guestName.empty}")
    @Size(min = 3, max = 100, message = "{guestName.boundaries}")
    private String guestName;

    @FutureOrPresent(message = "{startDate.inThePast}")
    @NotNull(message = "{startDate.empty}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @FutureOrPresent(message = "{endDate.inThePast}")
    @NotNull(message = "{endDate.empty}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    public EditReservationDetails(Reservation reservation) {
        this.roomId = reservation.getRoom().getId();
        this.reservationId = reservation.getId();
        this.guestName = reservation.getGuestName();
        this.startDate = reservation.getStartDate();
        this.endDate = reservation.getEndDate();
    }

    public EditReservationDetails() {
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
