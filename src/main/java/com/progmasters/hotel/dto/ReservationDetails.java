package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Reservation;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ReservationDetails {

    private Long id;

    private String guestName;

    private LocalDate startDate;

    private LocalDate endDate;

    public ReservationDetails() {
    }

    public ReservationDetails(Reservation reservation) {
        this.id = reservation.getId();
        this.guestName = reservation.getGuestName();
        this.startDate = reservation.getStartDate();
        this.endDate = reservation.getEndDate();
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
