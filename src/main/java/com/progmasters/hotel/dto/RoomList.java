package com.progmasters.hotel.dto;

import java.util.List;

public class RoomList {
    private List<RoomListItem> rooms;

    public RoomList(List<RoomListItem> roomListItems) {
        this.rooms = roomListItems;
    }

    public List<RoomListItem> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomListItem> rooms) {
        this.rooms = rooms;
    }
}
