package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Room;

public class RoomDetails {

    private Long id;

    private Long hotelId;

    private String hotelName;

    private String name;

    private Integer numberOfBeds;

    private Integer pricePerNight;

    private String description;

    private String imageUrl;

    public RoomDetails() {}

    public RoomDetails(Room room) {
        this.id = room.getId();
        this.hotelId = room.getHotel().getId();
        this.hotelName = room.getHotel().getName();
        this.name = room.getName();
        this.numberOfBeds = room.getNumberOfBeds();
        this.pricePerNight = room.getPricePerNight();
        this.description = room.getDescription();
        this.imageUrl = room.getImageUrl();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public Integer getPricePerNight() {
        return pricePerNight;
    }

    public void setPricePerNight(Integer pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }
}
