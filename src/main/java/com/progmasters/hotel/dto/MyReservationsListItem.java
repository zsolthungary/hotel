package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Reservation;

import java.time.LocalDate;

public class MyReservationsListItem {
    private Long reservationId;
    private String hotelName;
    private String roomName;
    private LocalDate startDate;
    private LocalDate endDate;


    public MyReservationsListItem(Reservation reservation) {
        this.reservationId = reservation.getId();
        this.hotelName = reservation.getRoom().getHotel().getName();
        this.roomName = reservation.getRoom().getName();
        this.startDate = reservation.getStartDate();
        this.endDate = reservation.getEndDate();
    }

    public MyReservationsListItem() {}

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
