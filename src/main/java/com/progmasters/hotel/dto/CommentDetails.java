package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Comment;

import java.time.format.DateTimeFormatter;

public class CommentDetails {

    private String author;

    private String text;

    private String createdAt;

    private Integer rating;

    public CommentDetails() {
    }

    public CommentDetails(Comment comment) {
        this.author = comment.getAuthor();
        this.text = comment.getText();
        this.createdAt = comment.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.rating = comment.getRating();
    }


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
