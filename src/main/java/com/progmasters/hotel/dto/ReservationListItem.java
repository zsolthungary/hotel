package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Reservation;

import java.time.LocalDate;

public class ReservationListItem {

    private Long id;
    private String guestName;
    private LocalDate startDate;
    private LocalDate endDate;


    public ReservationListItem() {
    }

    public ReservationListItem(Reservation reservation) {
        this.id = reservation.getId();
        this.guestName = reservation.getGuestName();
        this.startDate = reservation.getStartDate();
        this.endDate = reservation.getEndDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
