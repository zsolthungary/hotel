package com.progmasters.hotel.dto;

import java.util.List;

public class HotelList {
    private List<HotelListItem> hotels;

    public HotelList(List<HotelListItem> hotels) {
        this.hotels = hotels;
    }

    public List<HotelListItem> getHotels() {
        return hotels;
    }

    public void setHotels(List<HotelListItem> hotels) {
        this.hotels = hotels;
    }
}
