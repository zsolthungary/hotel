package com.progmasters.hotel.dto;

import java.util.List;

public class CalendarDetails {
    private int currentMonth;
    private int monthNumber;
    private int firstDayOfMonthOfYear;
    private int numberOfToday;
    private String nameOfMonth;
    private List<List<String[]>> month;

    public CalendarDetails(int currentMonth, int monthNumber, int firstDayOfMonthOfYear, int numberOfToday,  String nameOfMonth, List<List<String[]>> month) {
        this.currentMonth = currentMonth;
        this.monthNumber = monthNumber;
        this.firstDayOfMonthOfYear = firstDayOfMonthOfYear;
        this.numberOfToday = numberOfToday;
        this.nameOfMonth = nameOfMonth;
        this.month = month;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(int currentMonth) {
        this.currentMonth = currentMonth;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getFirstDayOfMonthOfYear() {
        return firstDayOfMonthOfYear;
    }

    public void setFirstDayOfMonthOfYear(int firstDayOfMonthOfYear) {
        this.firstDayOfMonthOfYear = firstDayOfMonthOfYear;
    }

    public int getNumberOfToday() {
        return numberOfToday;
    }

    public void setNumberOfToday(int numberOfToday) {
        this.numberOfToday = numberOfToday;
    }

    public List<List<String[]>> getMonth() {
        return month;
    }

    public void setMonth(List<List<String[]>> month) {
        this.month = month;
    }

    public String getNameOfMonth() {
        return nameOfMonth.toLowerCase();
    }

    public void setNameOfMonth(String nameOfMonth) {
        this.nameOfMonth = nameOfMonth;
    }
}