package com.progmasters.hotel.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RoomForm {

    @NotNull(message = "{roomName.empty}")
    @Size(min = 1, max = 200, message = "{roomName.boundaries}")
    private String name;

    @NotNull(message = "{numberOfBeds.boundaries}")
    @Min(value = 1, message = "{numberOfBeds.boundaries}")
    @Max(value = 50, message = "{numberOfBeds.boundaries}")
    private Integer numberOfBeds;

    @NotNull(message = "{pricePerNight.boundaries}")
    @Min(value = 1, message = "{pricePerNight.boundaries}")
    @Max(value = 1000000, message = "{pricePerNight.boundaries}")
    private Integer pricePerNight;

    private String description;

    private String imageUrl;

    @NotNull(message = "{hotelId.empty}")
    private Long hotelId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public Integer getPricePerNight() {
        return pricePerNight;
    }

    public void setPricePerNight(Integer pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }
}
