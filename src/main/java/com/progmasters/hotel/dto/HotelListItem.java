package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.Hotel;

public class HotelListItem {

    private Long id;

    private String name;

    private String address;

    private String imageUrl;

    private Integer numberOfRooms;

    private Double averageRating;

    private boolean hasEmptyRoom = true;

    public HotelListItem() {
    }

    public HotelListItem(Hotel hotel) {
        this.id = hotel.getId();
        this.name = hotel.getName();
        this.address = hotel.getAddress();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public boolean isHasEmptyRoom() {
        return hasEmptyRoom;
    }

    public void setHasEmptyRoom(boolean hasEmptyRoom) {
        this.hasEmptyRoom = hasEmptyRoom;
    }
}
