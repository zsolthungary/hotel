package com.progmasters.hotel.dto;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class CommentCommand {

    private Long hotelId;

    private String author;

    @NotBlank(message = "{comment.empty}")
    @Size(max = 1000, message = "{comment.boundaries}")
    @Column(columnDefinition="text")
    private String text;

    @Min(value = 1, message = "{rating.boundaries}")
    @Max(value = 5,  message = "{rating.boundaries}")
    private Integer rating;

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
