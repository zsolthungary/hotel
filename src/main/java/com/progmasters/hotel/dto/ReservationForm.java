package com.progmasters.hotel.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class ReservationForm {

    @NotNull(message = "{guestName.empty}")
    @Size(min = 3, max = 100, message = "{guestName.boundaries}")
    private String guestName;


    @FutureOrPresent(message = "{startDate.inThePast}")
    @NotNull(message = "{startDate.empty}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;


    @FutureOrPresent(message = "{endDate.inThePast}")
    @NotNull(message = "{endDate.empty}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    @NotNull(message = "{reservation.room.empty}")
    private Long roomId;

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
}
