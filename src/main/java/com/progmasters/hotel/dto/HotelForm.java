package com.progmasters.hotel.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class HotelForm {
    @NotNull(message = "{hotelName.empty}")
    @Size(min = 1, max = 200, message = "{hotelName.boundaries}")
    private String name;

    @NotNull(message = "{hotelAddress.empty}")
    @Size(min = 1, max = 200, message = "{hotelAddress.boundaries}")
    private String address;

    private String description;

    private List<String> imageUrls;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }
}
