package com.progmasters.hotel.dto;

import com.progmasters.hotel.domain.RegisterUser;

import javax.validation.constraints.NotEmpty;

public class RegisterUserDetails {

    private Long userId;

    @NotEmpty(message = "Field can not be empty!")
    private String userName;

    @NotEmpty(message = "Field can not be empty!")
    private String email;

    @NotEmpty(message = "Field can not be empty!")
    private String password;

    @NotEmpty(message = "Field can not be empty!")
    private String retypedPassword;

    public RegisterUserDetails() {
    }

    public RegisterUserDetails(RegisterUser registerUser) {
        this.userName = registerUser.getUserName();
        this.email = registerUser.getEmail();
        this.password = registerUser.getPassword();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}