package com.progmasters.hotel.dto;

import java.util.List;

public class CalendarReservationDetails {
    private Long roomId;
    private String guestName;
    private int firstDayOfSendedMonth;
    private int currentMonth;
    private List<List<String[]>> sendedMonth;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public int getFirstDayOfSendedMonth() {
        return firstDayOfSendedMonth;
    }

    public void setFirstDayOfSendedMonth(int firstDayOfSendedMonth) {
        this.firstDayOfSendedMonth = firstDayOfSendedMonth;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(int currentMonth) {
        this.currentMonth = currentMonth;
    }

    public List<List<String[]>> getSendedMonth() {
        return sendedMonth;
    }

    public void setSendedMonth(List<List<String[]>> sendedMonth) {
        this.sendedMonth = sendedMonth;
    }
}
