package com.progmasters.hotel.dto;

import javax.validation.constraints.NotEmpty;

public class RegisterDetails {

    @NotEmpty(message = "Field can not be empty!")
    private String userName;

    @NotEmpty(message = "Field can not be empty!")
    private String email;

    @NotEmpty(message = "Field can not be empty!")
    private String password;

    @NotEmpty(message = "Field can not be empty!")
    private String retypedPassword;



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }
}
