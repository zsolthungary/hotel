package com.progmasters.hotel.dto;

import java.util.List;

public class HotelDetails {

    private Long id;

    private String name;

    private String address;

    private String description;

    private Double averageRating;

    private List<String> imageUrls;

    private List<RoomListItem> rooms;

    private List<CommentDetails> comments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<RoomListItem> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomListItem> rooms) {
        this.rooms = rooms;
    }

    public List<CommentDetails> getComments() {
        return comments;
    }

    public void setComments(List<CommentDetails> comments) {
        this.comments = comments;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }
}
