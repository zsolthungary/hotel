package com.progmasters.hotel.dto;

import java.util.List;

public class ReservationList {

    private List<ReservationListItem> reservations;

    public ReservationList(List<ReservationListItem> reservations) {
        this.reservations = reservations;
    }

    public List<ReservationListItem> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationListItem> reservations) {
        this.reservations = reservations;
    }
}
