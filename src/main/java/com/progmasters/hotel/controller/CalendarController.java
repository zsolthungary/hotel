package com.progmasters.hotel.controller;

import com.progmasters.hotel.dto.CalendarDetails;
import com.progmasters.hotel.dto.CalendarReservationDetails;
import com.progmasters.hotel.service.CalendarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/calendar")
public class CalendarController {

    private CalendarService calendarService;

    public CalendarController(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @GetMapping("/{roomId}")
    public ResponseEntity<CalendarDetails> getInformation(@PathVariable("roomId") Long roomId){
        CalendarDetails calendarDetails = calendarService.getCalendarDetails(roomId, 0);
        return new ResponseEntity<>(calendarDetails, HttpStatus.OK);
    }

    @GetMapping("/{roomId}/month/{monthNumber}")
    public ResponseEntity<CalendarDetails> getInformation(@PathVariable("roomId") Long roomId, @PathVariable("monthNumber") int monthNumber){
        CalendarDetails calendarDetails = calendarService.getCalendarDetails(roomId, monthNumber);
        return new ResponseEntity<>(calendarDetails, HttpStatus.OK);
    }

    @PostMapping("/reservation/{userName}")
    public ResponseEntity reserveThroughCalendar(@RequestBody CalendarReservationDetails calendarReservationDetails, @PathVariable("userName") String userName){
        calendarService.createReservationThroughCalendar(calendarReservationDetails, userName);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
