package com.progmasters.hotel.controller;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.dto.*;
import com.progmasters.hotel.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Map;

@RestController
@RequestMapping("/api/hotels")
public class HotelController {

    private HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @GetMapping()
    public ResponseEntity<?> hotels(
            @RequestParam(required = false) String search,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to)
    {

        HotelList hotelList = hotelService.getHotelList(search, from, to);

        ResponseEntity<?> result;
        if (hotelList == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity<>(hotelList, HttpStatus.OK);
        }
        return result;
    }

    @GetMapping("/list")
    public  ResponseEntity<?> hotelList() {
        Map<Long, String> hotelList = hotelService.getHotelIdAndNameList();

        ResponseEntity<?> result;
        if (hotelList == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity<>(hotelList, HttpStatus.OK);
        }

        return result;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> hotelDetails(@PathVariable("id") Long id) {
        HotelDetails hotelDetails = hotelService.getHotelDetails(id);

        ResponseEntity<?> result;
        if (hotelDetails == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity<>(hotelDetails, HttpStatus.OK);
        }

        return result;
    }

    @PostMapping
    public ResponseEntity<?> createHotel(@RequestBody @Valid HotelForm hotelForm) {
        Hotel hotel = hotelService.createHotel(hotelForm);

        return new ResponseEntity<>(hotel.getId(), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteHotel(@PathVariable Long id) {
        boolean isDeleteSuccessfull = hotelService.deleteHotel(id);

        ResponseEntity<?> result;
        if (isDeleteSuccessfull) {
            result = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateHotel(@Valid @RequestBody HotelForm hotelForm, @PathVariable Long id) {
        Hotel updatedHotel = hotelService.updateHotel(hotelForm, id);
        ResponseEntity<RoomForm> result;

        if (updatedHotel == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity(updatedHotel.getId(), HttpStatus.OK);
        }

        return result;
    }
}
