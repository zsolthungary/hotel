package com.progmasters.hotel.controller;

import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.*;
import com.progmasters.hotel.service.ReservationService;
import com.progmasters.hotel.service.RoomService;
import com.progmasters.hotel.validation.ReservationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/rooms")
public class RoomController {

    private RoomService roomService;


    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping
    public RoomList rooms() {
        return roomService.getRoomList();
    }

    @GetMapping("/{id}")
    public RoomDetails roomDetail(@PathVariable("id") Long id) {
        return roomService.getRoomDetails(id);
    }


    @PostMapping
    public ResponseEntity<?> createRoom(@RequestBody @Valid RoomForm roomForm) {
        Long roomId = roomService.createRoom(roomForm);

        return new ResponseEntity(roomId, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteRoom(@PathVariable Long id) {
        boolean isDeleteSuccessfull = roomService.deleteRoom(id);

        ResponseEntity<?> result;
        if (isDeleteSuccessfull) {
            result = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateRoom(@Valid @RequestBody RoomForm roomForm, @PathVariable Long id) {
        Room updatedRoom = roomService.updateRoom(roomForm, id);
        ResponseEntity<RoomForm> result;

        if (updatedRoom == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity(updatedRoom.getId(), HttpStatus.OK);
        }

        return result;
    }
}
