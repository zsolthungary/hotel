package com.progmasters.hotel.controller;

import com.progmasters.hotel.domain.Reservation;
import com.progmasters.hotel.dto.EditReservationDetails;
import com.progmasters.hotel.dto.MyReservationsListItem;
import com.progmasters.hotel.dto.ReservationForm;
import com.progmasters.hotel.dto.ReservationList;
import com.progmasters.hotel.service.ReservationService;
import com.progmasters.hotel.validation.ReservationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rooms")
public class ReservationController {

    private ReservationService reservationService;
    private ReservationFormValidator reservationFormValidator;


    @Autowired
    public ReservationController(ReservationService reservationService, ReservationFormValidator reservationFormValidator) {
        this.reservationService = reservationService;
        this.reservationFormValidator = reservationFormValidator;
    }

    @InitBinder("reservationForm")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(reservationFormValidator);
    }


    @GetMapping("/{id}/reservations")
    public ReservationList getReservations(@PathVariable("id") Long id) {
        return reservationService.getReservationsByRoomId(id);
    }

    @PostMapping("/{id}/reservations/{userName}")
    public ResponseEntity createReservation(@RequestBody @Valid ReservationForm reservationForm, @PathVariable String userName) {
        reservationService.createReservation(reservationForm, userName);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/reservations/{reservationId}")
    public ResponseEntity<?> deleteReservation(@PathVariable("reservationId") Long id) {
        boolean isDeleteSuccessful = reservationService.deleteReservation(id);

        ResponseEntity<?> result;
        if (isDeleteSuccessful) {
            result = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return result;
    }

    @DeleteMapping("/myReservations/{reservationId}")
    public ResponseEntity<?> deleteMyReservation(@PathVariable("reservationId") Long id) {
        boolean isDeleteSuccessful = reservationService.deleteReservation(id);

        ResponseEntity<?> result;
        if (isDeleteSuccessful) {
            result = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return result;
    }

    @PutMapping("/reservations/{id}")
    public ResponseEntity<EditReservationDetails> updateReservation(@Valid @RequestBody EditReservationDetails editReservationDetails, @PathVariable Long id) {
        Reservation updatedReservation = reservationService.updateReservation(editReservationDetails, id);
        ResponseEntity<EditReservationDetails> result;

        if (updatedReservation == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity<>(new EditReservationDetails(updatedReservation), HttpStatus.OK);
        }

        return result;
    }

    @PutMapping("/myReservations/{id}")
    public ResponseEntity<EditReservationDetails> updateMyReservation(@Valid @RequestBody EditReservationDetails editReservationDetails, @PathVariable Long id) {
        Reservation updatedReservation = reservationService.updateReservation(editReservationDetails, id);
        ResponseEntity<EditReservationDetails> result;

        if (updatedReservation == null) {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            result = new ResponseEntity<>(new EditReservationDetails(updatedReservation), HttpStatus.OK);
        }

        return result;
    }

    @GetMapping("/reservations/{id}")
    public ResponseEntity<EditReservationDetails> getReservation(@PathVariable("id") Long id) {

        EditReservationDetails editReservationDetails = new EditReservationDetails(reservationService.getReservation(id));
        return new ResponseEntity<EditReservationDetails>(editReservationDetails, HttpStatus.OK);
    }

    @GetMapping("/myReservations/{userName}")
    public ResponseEntity<List<MyReservationsListItem>> getMyReservations(@PathVariable("userName") String userName) {
        List<MyReservationsListItem> myReservationsListItems = reservationService.getMyReservations(userName);
        return new ResponseEntity<>(myReservationsListItems, HttpStatus.OK);
    }


}
