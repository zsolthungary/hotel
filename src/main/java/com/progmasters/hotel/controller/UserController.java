package com.progmasters.hotel.controller;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.dto.HotelDetails;
import com.progmasters.hotel.dto.RegisterDetails;
import com.progmasters.hotel.dto.RegisterUserDetails;
import com.progmasters.hotel.security.AuthenticatedUserDetails;
import com.progmasters.hotel.service.RegisterService;
import com.progmasters.hotel.validation.RegisterUserDetailsValidator;
import com.progmasters.hotel.validation.RegistrationFormValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private RegisterService registerService;
    private RegistrationFormValidator registrationFormValidator;
    private RegisterUserDetailsValidator registerUserDetailsValidator;

    public UserController(RegisterService registerService, RegistrationFormValidator registrationFormValidator, RegisterUserDetailsValidator registerUserDetailsValidator) {
        this.registerService = registerService;
        this.registrationFormValidator = registrationFormValidator;
        this.registerUserDetailsValidator = registerUserDetailsValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(registrationFormValidator);
    }

//    @InitBinder("registrationFormEdit")
//    protected void initBinder2(WebDataBinder binder) {
//        binder.addValidators(registerUserDetailsValidator);
//    }

    @GetMapping("/me")
    public ResponseEntity<AuthenticatedUserDetails> getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user = (UserDetails) authentication.getPrincipal();

        return new ResponseEntity<>(new AuthenticatedUserDetails(user), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Valid RegisterDetails registerDetails) {
        registerService.register(registerDetails);
        return new ResponseEntity(HttpStatus.CREATED);
    }

//    @PutMapping("/edit-me/{id}")
//    public ResponseEntity updateUser(@RequestBody @Valid RegisterUserDetails registerUserDetails, @PathVariable Long id) {
//        RegisterUser updatedRegisterUser = registerService.updateRegisterUser(registerUserDetails, id);
//        ResponseEntity<RegisterUser> result;
//
//        if (updatedRegisterUser == null) {
//            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        } else {
//            result = new ResponseEntity(updatedRegisterUser.getId(), HttpStatus.OK);
//        }
//        return result;
//    }

}
