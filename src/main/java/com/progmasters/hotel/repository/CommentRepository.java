package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.Comment;
import com.progmasters.hotel.domain.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByHotelOrderByCreatedAtAsc(Hotel hotel);

    @Query("select avg(c.rating) from Comment c where c.hotel= :hotel")
    Double averageRating(@Param("hotel") Hotel hotel);
}
