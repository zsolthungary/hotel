package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    List<Room> findAllByHotelOrderByPricePerNightAsc(Hotel hotel);

    @Query("select count(r) from Room r where r.hotel= :hotel")
    Integer getRoomNumberByHotel(@Param("hotel") Hotel hotel);

    @Query("select r.id from Room r where r.hotel.id= :hotelId")
    List<Long> findAllIdByHotelId(@Param("hotelId") Long hotelId);
}
