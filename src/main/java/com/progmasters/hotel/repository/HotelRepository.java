package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long>, JpaSpecificationExecutor<Hotel> {

    @Query("select h from Hotel h where  h.address like %:search% or h.description like %:search% or h.name like %:search%")
    List<Hotel> findAllBySearch(@Param("search") String search);
}
