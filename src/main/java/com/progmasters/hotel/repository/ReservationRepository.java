package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.RegisterUser;
import com.progmasters.hotel.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> getReservationsByRoomIdOrderByStartDateAsc(Long roomId);
    List<Reservation> getReservationsByRoomId(Long roomId);
    List<Reservation> getReservationByRoomIdOrderByEndDateDesc (Long roomId);
    List<Reservation> findAllByRegisterUser(RegisterUser registerUser);


    @Query("select r.startDate, r.endDate from Reservation r where r.room.id= :id")
    List<Object[]> findDatesByUserId(@Param("id") Long id);

    List<Reservation> getAllByRoomId(Long roomId);
}
