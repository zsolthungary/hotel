package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findAllByHotelOrderByIdAsc(Hotel hotel);

    @Query("select i.url from Image i where i.hotel= :hotel order by i.id asc")
    List<String> findFirstImageByHotel(@Param("hotel")Hotel hotel);
}
