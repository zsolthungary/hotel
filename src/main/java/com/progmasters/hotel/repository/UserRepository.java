package com.progmasters.hotel.repository;

import com.progmasters.hotel.domain.RegisterUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<RegisterUser, Long> {

    RegisterUser findByUserName(String userName);
    RegisterUser findByEmail(String emailAddress);

}
