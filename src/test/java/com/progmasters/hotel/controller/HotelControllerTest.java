package com.progmasters.hotel.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.dto.HotelList;
import com.progmasters.hotel.dto.HotelListItem;
import com.progmasters.hotel.exception.GlobalExceptionHandler;
import com.progmasters.hotel.service.HotelService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(MockitoExtension.class)
public class HotelControllerTest {

    private MockMvc mockMvc;

    @Mock
    private HotelService hotelServiceMock;

    @InjectMocks
    private HotelController hotelController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(hotelController)
                .setControllerAdvice(new GlobalExceptionHandler(messageSource()))
                .build();
    }

    @AfterEach
    public void validate() {
        validateMockitoUsage();
    }

    @Test
    public void testGetHotels() throws Exception {
//        // given
//        Hotel hotel1 = new Hotel();
//        hotel1.setName("Hermitage");
//        hotel1.setAddress("Maldives");
//
//        Hotel hotel2 = new Hotel();
//        hotel2.setName("Ramada");
//        hotel2.setAddress("Berlin");
//
//        List<HotelListItem> hotelListItems = Arrays.asList(hotel1, hotel2).stream()
//                .map(HotelListItem::new).collect(Collectors.toList());
//
//
//        HotelList hotelList = new HotelList(hotelListItems);
//
//        // when
//        when(hotelServiceMock.getHotelList(null)).thenReturn(hotelList);
//
//        // then
//        this.mockMvc.perform(get("/api/hotels"))
////                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.hotels", hasSize(2)))
//                .andExpect(jsonPath("$.hotels[0].name", is("Hermitage")))
//                .andExpect(jsonPath("$.hotels[0].address", is("Maldives")))
//                .andExpect(jsonPath("$.hotels[1].name", is("Ramada")))
//                .andExpect(jsonPath("$.hotels[1]address", is("Berlin")));
//
//        verify(hotelServiceMock, times(1)).getHotelList(null);
//        verifyNoMoreInteractions(hotelServiceMock);
    }

    private MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

        messageSource.setBasename("messages");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
