package com.progmasters.hotel.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.progmasters.hotel.config.SpringWebConfig;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.RoomForm;
import com.progmasters.hotel.dto.RoomList;
import com.progmasters.hotel.dto.RoomListItem;
import com.progmasters.hotel.service.RoomService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



import java.util.ArrayList;
import java.util.List;


@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, RoomControllerTest.TestConfiguration.class})
public class RoomControllerTest {


    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    RoomService roomServiceMock;

    @BeforeEach
    public void setup() {
        Mockito.reset(roomServiceMock);

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @AfterEach
    public void validate() {
        validateMockitoUsage();
    }

    @Test
    public void testGetRoomList() throws Exception {
        Room room1 = new Room();
        room1.setPricePerNight(1500);
        room1.setNumberOfBeds(5);
        room1.setName("Standard Twin Room");

        Room room2 = new Room();
        room2.setPricePerNight(1800);
        room2.setNumberOfBeds(2);
        room2.setName("Standard Room");

        List<Room> roomList = new ArrayList<>();

        roomList.add(room1);
        roomList.add(room2);

        List<RoomListItem> roomListItems = new ArrayList<>();
        for (Room room : roomList) {
            RoomListItem roomListItem = new RoomListItem();
            roomListItem.setPricePerNight(room.getPricePerNight());
            roomListItem.setNumberOfBeds(room.getNumberOfBeds());
            roomListItem.setName(room.getName());
            roomListItems.add(roomListItem);
        }
        System.out.println("LIST: " + roomListItems);

        RoomList roomList1 = new RoomList(roomListItems);

        when(roomServiceMock.getRoomList()).thenReturn(roomList1);

        this.mockMvc.perform(get("/api/rooms"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andDo(print())
                .andExpect(jsonPath("$.rooms", hasSize(2)))
                .andExpect(jsonPath("$.rooms[0].pricePerNight", is(1500)))
                .andExpect(jsonPath("$.rooms[0].numberOfBeds", is(5)))
                .andExpect(jsonPath("$.rooms[0].name", is("Standard Twin Room")))
                .andExpect(jsonPath("$.rooms[1].pricePerNight", is(1800)))
                .andExpect(jsonPath("$.rooms[1].numberOfBeds", is(2)))
                .andExpect(jsonPath("$.rooms[1].name", is("Standard Room")));
    }

    @Test
    public void testPostRoom() throws Exception {
        Room room = new Room();
        room.setPricePerNight(1500);
        room.setNumberOfBeds(5);
        room.setName("Standard Double Room");


        RoomForm roomForm = new RoomForm();
        roomForm.setPricePerNight(room.getPricePerNight());
        roomForm.setNumberOfBeds(room.getNumberOfBeds());
        roomForm.setName(room.getName());


        this.mockMvc.perform(post("/api/rooms")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(roomForm)))
                .andExpect(status().is(201));

    }

    @Test
    public void testDeleteRoom() throws Exception {
        Room room = new Room();
        room.setId(1L);
        room.setPricePerNight(1500);
        room.setNumberOfBeds(5);
        room.setName("Standard Double Room");


        RoomForm roomForm = new RoomForm();
        roomForm.setPricePerNight(room.getPricePerNight());
        roomForm.setNumberOfBeds(room.getNumberOfBeds());
        roomForm.setName(room.getName());

        when(roomServiceMock.deleteRoom(room.getId())).thenReturn(true);

        this.mockMvc.perform(delete("/api/rooms/{id}", room.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(roomForm)))
                .andExpect(status().is(204));

    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Configuration
    static class TestConfiguration {

        @Bean
        public RoomService roomService() {
            return Mockito.mock(RoomService.class);
        }
    }
}

