package com.progmasters.hotel.repository;

import com.progmasters.hotel.config.SpringWebConfig;
import com.progmasters.hotel.config.TestConfiguration;
import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.RoomForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, TestConfiguration.class})
public class RoomRepositoryTest {

    @Autowired
    private RoomRepository roomRepository;

    @Test
    public void testSaveAndFindAll(){


        Room room = new Room();
        room.setId(1L);
        room.setPricePerNight(1500);
        room.setNumberOfBeds(5);
        room.setName("Standard Double Room");

        RoomForm roomForm = new RoomForm();
        roomForm.setPricePerNight(room.getPricePerNight());
        roomForm.setNumberOfBeds(room.getNumberOfBeds());
        roomForm.setName(room.getName());

        roomRepository.save(room);

        List<Room> rooms = roomRepository.findAll();

        assertEquals(1, rooms.size());
    }
}
