package com.progmasters.hotel.repository;

import com.progmasters.hotel.config.SpringWebConfig;
import com.progmasters.hotel.config.TestConfiguration;
import com.progmasters.hotel.domain.Hotel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, TestConfiguration.class})
public class HotelRepositoryTest {

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void testSaveAndFindAll() {
        // given
        Hotel hotel = new Hotel();
        hotel.setName("Ramada");
        hotel.setDescription("Great place");
        hotel.setAddress("Paris");

        hotelRepository.save(hotel);

        // when
        List<Hotel> orcs = hotelRepository.findAll();

        // then
        assertEquals(1, orcs.size());
    }

}
