package com.progmasters.hotel.repository;

import com.progmasters.hotel.config.SpringWebConfig;
import com.progmasters.hotel.config.TestConfiguration;
import com.progmasters.hotel.domain.Reservation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, TestConfiguration.class})
public class ReservationRepositoryTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void testSaveAndFindAll() {

        Reservation reservation = new Reservation();
        reservation.setGuestName("András");

        String startDate = "2019-05-23";
        DateTimeFormatter startDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        reservation.setStartDate(LocalDate.parse(startDate, startDateFormatter));

        String endDate = "2019-05-26";
        DateTimeFormatter endDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        reservation.setEndDate(LocalDate.parse(endDate, endDateFormatter));


        reservationRepository.save(reservation);

        List<Reservation> reservations = reservationRepository.findAll();

        assertEquals(1, reservations.size());
    }

    @Test
    public void testDelete() {

        Reservation reservation = new Reservation();
        reservation.setGuestName("András");

        String startDate = "2019-05-23";
        DateTimeFormatter startDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        reservation.setStartDate(LocalDate.parse(startDate, startDateFormatter));

        String endDate = "2019-05-26";
        DateTimeFormatter endDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        reservation.setEndDate(LocalDate.parse(endDate, endDateFormatter));

        reservationRepository.delete(reservation);

        List<Reservation> reservations = reservationRepository.findAll();

        assertEquals(0, reservations.size());
    }

}
