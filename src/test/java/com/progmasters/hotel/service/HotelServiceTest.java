package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.dto.HotelForm;
import com.progmasters.hotel.repository.HotelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class HotelServiceTest {

    private HotelService hotelService;

    private HotelRepository hotelRepositoryMock;

//    @BeforeEach
//    public void setUp() {
//        hotelRepositoryMock = mock(HotelRepository.class);
//        hotelService = new HotelService(hotelRepositoryMock, null, null, null);
//    }
//
//    @Test
//    public void testSavingHotel() {
//        // given
//        HotelForm hotelForm = new HotelForm();
//        hotelForm.setName("Ramada");
//        hotelForm.setAddress("Paris");
//        hotelForm.setDescription("Great place");
//
//        Hotel hotel = new Hotel(hotelForm);
//
//        // when
//        when(hotelRepositoryMock.save(any(Hotel.class))).thenAnswer(returnsFirstArg());
//        //when(hotelRepositoryMock.findOne(any(Long.class))).thenReturn(new Orc());
//
//        Hotel savedHotel = hotelService.createHotel(hotelForm);
//
//        // then
//        assertEquals(hotelForm.getName(), savedHotel.getName());
//        assertEquals(hotelForm.getAddress(), savedHotel.getAddress());
//        assertEquals(hotelForm.getDescription(), savedHotel.getDescription());
//
//        verify(hotelRepositoryMock, times(1)).save(any(Hotel.class));
//        verifyNoMoreInteractions(hotelRepositoryMock);
//    }
}
