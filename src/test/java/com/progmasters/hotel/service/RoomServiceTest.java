package com.progmasters.hotel.service;

import com.progmasters.hotel.domain.Hotel;
import com.progmasters.hotel.domain.Room;
import com.progmasters.hotel.dto.RoomForm;
import com.progmasters.hotel.repository.HotelRepositoryTest;
import com.progmasters.hotel.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RoomServiceTest {

    private RoomService roomService;

    private RoomRepository roomRepositoryMock;

    private HotelRepositoryTest hotelRepositoryMock;


    @BeforeEach
    public void setUp() {
        roomRepositoryMock = mock(RoomRepository.class);
        hotelRepositoryMock = mock(HotelRepositoryTest.class);
        //roomService = new RoomService(roomRepositoryMock, hotelRepositoryMock);
    }

    @Test
    public void testSavingRoom(){
//        Hotel hotel = new Hotel();
//        hotel.setId(2L);
//
//        Room room = new Room();
//        room.setId(1L);
//        room.setPricePerNight(1500);
//        room.setNumberOfBeds(5);
//        room.setName("Standard Double Room");
//        room.setHotel(hotel);
//
//        RoomForm roomForm = new RoomForm();
//        roomForm.setPricePerNight(room.getPricePerNight());
//        roomForm.setNumberOfBeds(room.getNumberOfBeds());
//        roomForm.setName(room.getName());
//
//        when(roomRepositoryMock.save(any(Room.class))).thenAnswer(returnsFirstArg());
//
//        Long id = roomService.createRoom(roomForm);
//
//        assertEquals(room.getId(), id);
    }
}
